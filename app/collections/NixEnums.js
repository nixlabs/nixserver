"use strict";

if (Nix == undefined) {
    var Nix = {};
}

if (Nix.Enum == undefined) {
    Nix.Enum = {};
}

Nix.Enum.NixDataObjectStates = {
    New: "new",
    Loaded: "loaded",
    Updated: "updated",
    Deleted: "deleted"
};

Nix.Enum.DataContextStates = {
    New: "new",
    Requested: "requested",
    Error: "error",
    Completed: "completed",
};

Nix.Enum.FileNamingTypes = {
    Random: 'random',
    Incremental: 'incremental'
}

Nix.DataTypes = {
    String: 'string',
    Reference_Object: 'reference_object',
    Reference_Array: 'reference_array'
}

var module = module || {};
module.exports = Nix;