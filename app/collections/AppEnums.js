"use strict";

if (Nix == undefined) {
    var Nix = {};
}

if (Nix.AppEnum == undefined) {
    Nix.AppEnum = {};
}

Nix.AppEnum.OfferStates = {
    Draft: "draft",
    Sent: "sent",
    Canceled: "canceled"
};

Nix.AppEnum.InvoiceStates = {
    Draft: "draft",
    Sent: "sent",
    Payed: "payed",
    Expected: "expected",
    Canceled: "canceled"
};

Nix.AppEnum.ExpensesStates = {
    Draft: "draft",
    Expected: "expected",
    Received: "received",
    Payed: "payed",
    Canceled: "canceled"
};


Nix.AppEnum.TransactionTypes = {
    Internal: "internal",
    ExtToInter: "externaltointernal",
    InterToExt: "internaltoexternal",
};
Nix.AppEnum.PotentialCustomerStatus = {
    New: "New",
    Open: "Open",
    Rejected: "Rejected",
    Accepted:"Accepted"
};
Nix.AppEnum.CallLogStatus = {
    Answered:"answered",
    NotAnswered:"notanswered"
};

var module = module || {};
module.exports = Nix;