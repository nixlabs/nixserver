var extend = require('extend');

var Nix = {
};

var NixEnums = require('./NixEnums.js');
extend(Nix, NixEnums);

var NixAppEnums = require('./AppEnums.js');
extend(Nix, NixAppEnums);

module.exports = Nix;