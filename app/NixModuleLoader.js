"use strict";

var fs = require('fs');
var util = require('util');

module.exports = function () {
  this.AddModule = function (modulePath) {
    var tmpModule = require(modulePath);
    var initModule = new tmpModule();

    if (initModule.ModuleName == undefined) {
      throw("ModuleName missing.")
    }

    if(initModule.ModulePriority == undefined){
      initModule.ModulePriority = 0;
    }

    this.ModuleDic[initModule.ModuleName] = initModule;
    this.Modules.push(initModule);

    return initModule;
  };

  global.BasePath = process.cwd();

  /* Load Nix Structures and Configurations */
  var configHelper = require('./NixConfigHelper.js');
  var configHelperInstance = new configHelper();
  global.Nix = require('./collections/NixCollections.js');
  global.Nix.Config = require('config');
  var modulesFolder = global.Nix.Config.AppPath + '/modules';
  global.Nix.Modules = this.ModuleDic = {};
  this.Modules = [];

  /* Load Main Modules */
  global.RestifyHelper = this.AddModule("./services/restifyHelper.js");
  global.SocketHelper = this.AddModule("./services/socketWrapper.js");
  global.Nix.AppRegister = this.AddModule("./dal/NixAppRegister.js");

  if (Nix.Config.dbConfig.type == "sql") {
    global.Nix.SqlConnector = this.AddModule("./dal/sql/NixSqlConnector.js");
    global.Nix.DalCreator = this.AddModule("./dal/sql/NixDALCreator.js");
    global.Nix.DalManager = this.AddModule("./dal/sql/NixDALManager.js");
    global.Nix.DBManager = this.AddModule("./dal/sql/NixDBManager.js");
  }
  else if (Nix.Config.dbConfig.type == "mongo") {
    global.Nix.DalCreator = this.AddModule("./dal/mongo/NixDALCreator.js");
    global.Nix.DalManager = this.AddModule("./dal/mongo/NixDALManager.js");
  }

  global.Nix.UserModule = this.AddModule("./dal/NixUserModule.js");
  global.Nix.AuthModule = this.AddModule("./dal/NixAuthModule.js");
  global.Nix.CacheManager = this.AddModule("./dal/NixCacheManager.js");

    if(Nix.Config.dmsConfig)
    global.Nix.DmsManager = this.AddModule("./dms/dmsmanager.js");

  /* Load Dynamic Modules */
  var physicalFolder = __dirname + "\\" + modulesFolder;
  var relativeFolder = "../../." + modulesFolder + "/";

  var files = fs.readdirSync(modulesFolder);

  for (var i = 0; i < files.length; i++) {
    if (files[i] == "templateModule.js" || files[i].toLowerCase().endsWith('.js') == false) {
      continue;
    }
    else {
      console.info("Loading module " + files[i]);

      try {
        var initModule = this.AddModule(relativeFolder + files[i]);
      }
      catch (ex) {
        console.error("Error loading " + files[i] + ", error:" + ex);
      }
    }
  }


  console.info("Start All Modules")
  /* Initialize Modules */

  var loadedModulesCounter = 0;
  var tmpModule;


  var initModules = function (err) {
    if (err) {
      console.error("----ERROR ON MODULE INIT----");
      console.error("Module: " + tmpModule.ModuleName);
      console.error("Error: " + err);
      console.error("----STOP----");
      process.exit();
      return;
    }

    if (loadedModulesCounter < this.Modules.length) {
      tmpModule = this.Modules[loadedModulesCounter];
      loadedModulesCounter++;
      console.info(util.format("Init module %s/%s,  %s", loadedModulesCounter, this.Modules.length, tmpModule.ModuleName));

      if (tmpModule.Init) {
        var promise = tmpModule.Init();
        if (promise instanceof Promise) {
          promise.then(initModules, initModules);
        }
        else {
          initModules();
        }
      }
      else {
        initModules();
      }
    }
    else {
      console.log("----- Server Started -----");
    }
  }.bind(this);


  this.Modules.sort(function(a, b) {
    if(a.ModulePriority < b.ModulePriority){
      return -1;
    }else if(a.ModulePriority > b.ModulePriority){
      return 1;
    }
    return 0;
  });

  initModules();

}
