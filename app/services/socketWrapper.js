module.exports = function () {
  this.ModuleName = "SocketHelper";
  this.ModulePriority = -99;

  var server = global.RestifyHelper.Server;

  var socket = null;


  var Nix = global.Nix;

  if(Nix.Config.socketUrl && Nix.Config.socketUrl != null && Nix.Config.socketUrl.length > 0){
    socket = require('socket.io')(server.server, {path: Nix.Config.socketUrl});
    console.log("Socket IO running on "+ Nix.Config.socketUrl)
  }
  else{
    socket = require('socket.io');
  }



  var cookieParser = require('socket.io-cookie-parser');


  var dl = require('delivery');
  var fs = require('fs');




  var userSockets = {};

  var io = null;

  var socketioJwt = require("socketio-jwt");

  this.ExtServices = {};
  var ServiceInfos = [];
  this.AddService = function (srvInfo, callBack) {
    var serviceInfo = srvInfo;
    if (typeof(srvInfo) == "string") {
      serviceInfo = {path: srvInfo, description: "SocketIO Service", title: srvInfo, group: "other"};
    }

    this.ExtServices[serviceInfo.path] = callBack;
    ServiceInfos.push(serviceInfo);
  };

  this.GetServiceList = function () {
    return ServiceInfos;
  };

  this.EmitToUser = function (userID, methodName, data) {
    var socket = userSockets[userID];
    socket.emit(methodName, data);
  };

  this.BroadcastToAll = function (methodName, data) {
    io.sockets.emit(methodName, data);
  };

  this.GetActiveUsers = function () {
    return Object.keys(userSockets);
  };

  this.OnConnectionListerners = [];
  this.AddOnConnectionListener = function (listener) {
    this.OnConnectionListerners.push(listener);
  }.bind(this);
  this.NotifyOnConnectionListeners = function (socket) {
    for (var i = 0; i < this.OnConnectionListerners.length; i++) {
      this.OnConnectionListerners[i](socket);
    }
  }.bind(this);

  this.OnDisconnectListeners = [];
  this.AddOnDisconnectListeners = function (listener) {
    this.OnDisconnectListeners.push(listener);
  }.bind(this);
  this.NotifyOnDisconnectListeners = function (socket) {
    for (var i = 0; i < this.OnDisconnectListeners.length; i++) {
      this.OnDisconnectListeners[i](socket);
    }
  }.bind(this);


  this.Init = function () {

    io = socket.listen(server.server);

    io.use(cookieParser());
    io.use(socketioJwt.authorize({
      secret: Nix.Config.tokenSecret,
      handshake: true
    }));

    io.on('connection', function (socket) {
      try {
        socket.User = global.Nix.UserModule.GetUserByID(parseInt(socket.decoded_token.ID));
      } catch (ex) {
      }

      if (socket.User == undefined) {
        socket.disconnect();
        return;
      }
      userSockets[socket.User.ID] = socket;
      socket.emit('userobject', socket.User);

      this.NotifyOnConnectionListeners(socket);

      socket.on('disconnect', function () {
        this.NotifyOnDisconnectListeners(socket);
        delete userSockets[socket.User.ID];
      }.bind(this));

      socket.on('syncGDrive', function (obj, fn) {
        gdrive.RefreshTemplates(function (res) {
          fn(res);
        });
      });
      socket.on('addContractDocument', function (nixObjects, fn) {
        gdrive.AddNewContractDocument(nixObjects, function (res) {
          fn(res);
        });
      });

      function AppendService(socket, serviceName, serviceMethod) {
        socket.on(serviceName, function (a, b, c, d, e, f) {
          var args = [];
          for (var i = 0; i < arguments.length; i++) {
            args.push(arguments[i]);
          }

          args.push(socket.User);

          serviceMethod.apply(this, args);
        });
      }

      var services = Object.keys(this.ExtServices);
      for (var i = 0; i < services.length; i++) {
        AppendService(socket, services[i], this.ExtServices[services[i]]);
      }

      var delivery = dl.listen(socket);
      delivery.on('receive.success', function (file) {
        var params = file.params;

        fs.mkdir("uploads", function (e) {

          var path = "uploads/" + params.NixObjectType;

          fs.mkdir(path, function (e) {
            if (!e || (e && e.code === 'EEXIST')) {
              path = path + "/" + params.ObjectID;
              fs.mkdir(path, function (e) {

                fs.writeFile(path + "/" + file.name, file.buffer, function (err) {
                  if (err) {
                    console.log('File could not be saved.' + err);
                  } else {
                    console.log('File saved.');
                  }
                  ;
                });

              });

            } else {
              //debug
              console.log(e);
            }
          });
        });

      });

    }.bind(this));

  }.bind(this);

};
