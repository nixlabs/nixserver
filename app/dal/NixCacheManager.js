"use strict";

module.exports = function () {
    this.ModuleName = "NixCacheManager";
    this.ModulePriority = -77;

    this.CachingObjects = [];
    this.CachedData = {};

    var Nix = global.Nix;
    var dalManager = global.Nix.DalManager;
    var socketHelper = global.SocketHelper;

    this.AddObjectCacheHandler = function (nixObjectType) {
        dalManager.AddOnSubmitAfterHandler(nixObjectType, function (data, promiseResolve, promiseReject) {
            promiseResolve();
            this.PrepareCacheObject(nixObjectType, function(){
                var res = {};
                res[nixObjectType] = this.CachedData[nixObjectType];
                socketHelper.BroadcastToAll("cachedata", res);
            }.bind(this));
        }.bind(this));
    }.bind(this);

    this.PrepareCacheObject = function(nixObjectType, promiseResolve, promiseReject){
        var dc_data = dalManager.GetBlankDataContext(nixObjectType);
        dalManager.GetAllObjects(dc_data, function (res) {
            this.CachedData[nixObjectType] = {NixObjectType: nixObjectType, Data: dc_data.data };
            if(promiseResolve)
                promiseResolve();
        }.bind(this),true);
    }.bind(this);

    this.GetCachedData = function(){
        return this.CachedData;
    }.bind(this);


    this.InitCache = function(promiseResolve, promiseReject){
        for (var i = 0; i < this.CachingObject.length; i++) {
            this.AddObjectCacheHandler(this.CachingObject[i]);
        }


        var cacheIndex = -1;

        var AddSocketConnectionHandler = function(){
            socketHelper.AddOnConnectionListener(function(socket){
                socket.emit("cachedata", this.CachedData);
            }.bind(this));

            socketHelper.BroadcastToAll("cachedata", this.CachedData);

            promiseResolve();
        }.bind(this);

        var LoadNextCacheObject = function(callBack){
            cacheIndex++;
            if(cacheIndex < this.CachingObject.length){
                this.PrepareCacheObject(this.CachingObject[cacheIndex], LoadNextCacheObject);
            }
            else{
                AddSocketConnectionHandler();
            }
        }.bind(this);

        LoadNextCacheObject();

    }.bind(this);


    this.Init = function () {
        if(Array.isArray(Nix.Config.cachingModels) == false){
            console.error("Missing config for cached models. Check default.sample.json");
            return;
        }

        this.CachingObject = [];

        for (var i= 0; i < Nix.Config.cachingModels.length;i++)
        {
            if(Nix.DAL[Nix.Config.cachingModels[i]]){
                this.CachingObject.push(Nix.DAL[Nix.Config.cachingModels[i]].NixObjectType);
            }
            else{
                console.error("Invalid cache model: "+ Nix.Config.cachingModels[i]);
            }
        }

        var promise = new Promise(this.InitCache);
        return promise;
    }.bind(this);
}
