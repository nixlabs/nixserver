﻿var uuid = require('node-uuid');

module.exports = function (objectType, tableName) {
    tableName = tableName || objectType;

    this.NixObjectType = tableName.toLowerCase();

    this.TableName = tableName;

    this.TableStructure = null;

    var Nix = global.Nix;

    this.ObjectDefinition = function (tableStructure) {
        var fields = Object.keys(tableStructure);
        for (var i = 0; i < fields.length; i++) {
            this[fields[i]] = null;
        }
        ;
    }

    this.columnNames = [];

    this.GetColumnNames = function () {
        if (this.columnNames.length == 0) {
            this.columnNames = Object.keys(this.TableStructure);
        }
        return this.columnNames;
    }

    this.SetTableStructure = function (tableStructure) {
        this.TableStructure = tableStructure;
    }

    this.CreateReferenceStructure = function (reference, refModel) {
        var origName = tmpName = reference.FKTABLE_NAME;

        counter = 1;

        while (this.TableStructure[tmpName] != undefined) {
            tmpName = origName + counter;
            counter++;
        }

        this.TableStructure[tmpName] = {
            "index": -1,
            "name": tmpName,
            "length": 1,
            "nullable": true,
            "caseSensitive": false,
            "identity": false,
            "readOnly": false,
            "datatype": Nix.DataTypes.Reference_Array,
            "field": reference.PKCOLUMN_NAME,
            "fk_field": reference.FKCOLUMN_NAME,
            "model": reference.FKTABLE_NAME
        };

        refModel.CreateFKReferenceStructure(reference, this);

    }.bind(this);

    this.CreateFKReferenceStructure = function (reference, refModel) {
        var origName = reference.PKTABLE_NAME;
        origName = origName.substring(0, origName.length - 1);

        counter = 1;
        var tmpName = origName;

        while (this.TableStructure[tmpName] != undefined) {
            tmpName = origName + counter;
            counter++;
        }

        this.TableStructure[tmpName] = {
            "index": -1,
            "name": tmpName,
            "length": 1,
            "nullable": true,
            "caseSensitive": false,
            "identity": false,
            "readOnly": false,
            "datatype": Nix.DataTypes.Reference_Object,
            "field": reference.FKCOLUMN_NAME,
            "fk_field": reference.PKCOLUMN_NAME,
            "model": reference.PKTABLE_NAME
        };

    }

    this.CreateNewObject = function () {
        var newObject = new this.ObjectDefinition(this.TableStructure);

        newObject.NixObjectID = uuid.v1();
        newObject.NixObjectState = Nix.Enum.NixDataObjectStates.New;
        newObject.NixObjectType = this.NixObjectType;

        return newObject;
    }.bind(this);

    this.ObjectSize = function (Object) {
        size = 0;
        for (key in Object) {
            if (Object.hasOwnProperty(key))
                size++;
        }
        return size;
    }

    this.CreateSelectStatement = function (DataLoadOptions, Filters, OrderBy, Paging) {
        var fields = "";

        if (DataLoadOptions) {
            var columns = this.GetColumnNames();

            for (var i = 0; i < columns.length; i++) {
                if (DataLoadOptions.indexOf(columns[i]) >= 0 && this.TableStructure[columns[i]].datatype != Nix.DataTypes.Reference_Array && this.TableStructure[columns[i]].datatype != Nix.DataTypes.Reference_Object) {
                    if (fields.length > 0) {
                        fields += ", ";
                    }

                    fields += columns[i];
                }
            }
            ;
        }

        if (fields.length == 0)
            fields = "*";

        var whereStmt = "";

        if (this.ObjectSize(Filters.AND) > 0 || this.ObjectSize(Filters.OR) || Filters != null) {
            whereStmt = this.getWhereStatement(Filters);
        }
        else {
            if (typeof this.TableStructure.DeletedOn != 'undefined') {
                whereStmt = " WHERE DeletedOn is null";
            }
        }

        var orderByStmt = "";
        if (OrderBy && OrderBy != null) {
            orderByStmt = this.getOrderByStatement(OrderBy);
        }

        var pagging = "";
        if (Paging) {

            if (orderByStmt.length == 0) {
                orderByStmt = " order by id";
            }

            var pageSkip = Paging.page * Paging.size;
            pagging = " OFFSET " + pageSkip + " ROWS FETCH NEXT " + Paging.size + " ROWS ONLY;";
            pagging += " SELECT Count(*) as TotalItems FROM " + this.TableName + whereStmt + ";";

        }

        return "SELECT " + fields + " FROM " + this.TableName + whereStmt + orderByStmt + pagging;
    }

    this.CreateUpdateStatement = function (UpdateObject) {
        var tmpStmt = "UPDATE " + this.TableName;
        var updateStmt = "";
        var whereStmt = "";
        var fieldObjs = Object.keys(this.TableStructure);
        for (var i = 0; i < fieldObjs.length; i++) {
            var field = this.TableStructure[fieldObjs[i]];

            if (field.identity) {
                if (UpdateObject[field.name] != undefined) {
                    if (whereStmt.length > 0) {
                        whereStmt += " AND ";
                    }
                    if (field.datatype == "string") {
                        whereStmt += field.name + " = N'" + UpdateObject[field.name] + "'";
                    }
                    else {
                        whereStmt += field.name + " = '" + UpdateObject[field.name] + "'";
                    }
                }
            }
            else {
                if (UpdateObject[field.name] !== undefined) {
                    if (updateStmt.length > 0) {
                        updateStmt += ", ";
                    }

                    if (UpdateObject[field.name] === null) {
                        updateStmt += field.name + " = NULL";
                    }
                    else {
                        if (field.datatype == "string") {
                            updateStmt += field.name + " = N'" + UpdateObject[field.name] + "'";
                        }
                        else {
                            updateStmt += field.name + " = '" + UpdateObject[field.name] + "'";
                        }
                    }
                }
            }
        }

        return tmpStmt + " SET " + updateStmt + " WHERE " + whereStmt;
    };

    this.CreateInsertStatement = function (tmpObject) {

        var tmpStmt = "INSERT into " + this.TableName;
        var fieldStmt = "";
        var valueStmt = "";

        var fieldObjs = Object.keys(this.TableStructure);

        for (var i = 0; i < fieldObjs.length; i++) {
            var field = this.TableStructure[fieldObjs[i]];

            if (field.identity == false) {

                if (field.datatype != Nix.DataTypes.Reference_Array && field.datatype != Nix.DataTypes.Reference_Object) {

                    if (fieldStmt.length > 0) {
                        fieldStmt += ", ";
                        valueStmt += ", ";
                    }

                    fieldStmt += fieldObjs[i];
                    if (tmpObject[field.name] != null) {
                        if (field.datatype == "string") {
                            valueStmt += "N";
                        }

                        valueStmt += "'" + tmpObject[field.name] + "'";
                    }
                    else {
                        valueStmt += "Null";
                    }
                }
            }

        }


        return tmpStmt + " (" + fieldStmt + ") values (" + valueStmt + ");SELECT SCOPE_IDENTITY() as insertedID;";
    }

    this.CreateSelectByIDStatement = function (tmpObject) {
        return this.CreateSelectStatement(0, tmpObject.Filters);

    }

    this.CreateDeleteStatement = function (tmpObject) {
        return "delete from " + this.TableName + this.getWhereStatement({
                AND: {
                    ID: {
                        Value: tmpObject.ID,
                        Operator: "="
                    }
                }, OR: {}
            });
    }.bind(this);

    this.getWhereStatement = function (tmpObject) {
        var whereStmt = "";
        var res = [];
        var fieldObjs = Object.keys(this.TableStructure);

        if (tmpObject.AND == undefined && tmpObject.OR == undefined) {
            for (var i = 0; i < fieldObjs.length; i++) {
                var field = this.TableStructure[fieldObjs[i]];

                if (tmpObject[field.name] != undefined) {
                    if (whereStmt.length > 0) {
                        whereStmt += " AND ";
                    }
                    if (tmpObject[field.name] === null) {
                        whereStmt += field.name + " is null";
                    }
                    else {
                        if (field.datatype == "String" || field.datatype == "text") {
                            whereStmt += field.name + " Like N" + "'%" + tmpObject[field.name] + "%'";
                        }
                        else {
                            whereStmt += field.name + " = " + "'" + tmpObject[field.name] + "'";
                        }
                    }
                }
            }
        }
        else {
            var filterFields = Object.keys(tmpObject);
            for (var k = 0; k < filterFields.length; k++) {
                var tmpRes = "";

                if (tmpObject[filterFields[k]] != undefined && !(Object.getOwnPropertyNames(tmpObject[filterFields[k]]).length === 0)) {
                    for (var i = 0; i < fieldObjs.length; i++) {
                        var field = this.TableStructure[fieldObjs[i]];

                        if (tmpObject[filterFields[k]][field.name] !== undefined) {
                            if (tmpRes.length > 0) {
                                tmpRes += " " + filterFields[k] + " ";
                            }

                            if (tmpObject[filterFields[k]][field.name] === null) {
                                tmpRes += field.name + " is null";
                            }
                            else if (tmpObject[filterFields[k]][field.name].Value == "null") {
                                if (tmpObject[filterFields[k]][field.name].Operator == '=') {
                                    tmpRes += field.name + " is null";
                                }
                                else if (tmpObject[filterFields[k]][field.name].Operator == '!=') {
                                    tmpRes += field.name + " is not null";
                                }
                            }
                            else {
                                if (tmpObject[filterFields[k]][field.name].Value == undefined) {
                                    var pom = tmpObject[filterFields[k]][field.name];
                                    var op = Object.keys(tmpObject[filterFields[k]][field.name]);
                                    tmpRes += '(';
                                    for (var j = 0; j < pom[op[0]].length; j++) {
                                        var obj = pom[op[0]][j];
                                        if (obj.Operator == "Like N") {
                                            tmpRes += field.name + " " + obj.Operator + "'%" + obj.Value + "%'";
                                        }
                                        else {
                                            if (obj.Value == "NULL") {
                                                tmpRes += field.name + " " + obj.Operator + " " + obj.Value;
                                            }
                                            else {
                                                tmpRes += field.name + obj.Operator + "'" + obj.Value + "'";
                                            }
                                        }
                                        if (j != pom[op[0]].length - 1) tmpRes += op[0] + " ";
                                    }
                                    tmpRes += ')';
                                }
                                else {
                                    if (tmpObject[filterFields[k]][field.name].Operator == "Like N") {
                                        tmpRes += field.name + " " + tmpObject[filterFields[k]][field.name].Operator + "'%" + tmpObject[filterFields[k]][field.name].Value + "%'";
                                    }
                                    else if (tmpObject[filterFields[k]][field.name].Operator == "IN") {
                                        var tmpValues = "";
                                        for (var j = 0; j < tmpObject[filterFields[k]][field.name].Value.length; j++) {
                                            if (j > 0) {
                                                tmpValues += ', ';
                                            }

                                            tmpValues += tmpObject[filterFields[k]][field.name].Value[j];
                                        }

                                        tmpRes += field.name + " IN (" + tmpValues + ")";
                                    }
                                    else {
                                        if (tmpObject[filterFields[k]][field.name].Value == "NULL") {
                                            tmpRes += field.name + " " + tmpObject[filterFields[k]][field.name].Operator + " " + tmpObject[filterFields[k]][field.name].Value;
                                        }
                                        else {
                                            tmpRes += field.name + tmpObject[filterFields[k]][field.name].Operator + "'" + tmpObject[filterFields[k]][field.name].Value + "'";
                                        }
                                    }
                                }

                            }


                        }
                    }
                    ;
                }
                if (tmpRes != "") {
                    res.push(tmpRes);
                }
            }
        }

        for (var k = 0; k < res.length; k++) {
            if (k != 0) {
                whereStmt += " AND "
            }
            whereStmt += res[k];
        }

        if (typeof this.TableStructure.DeletedOn != 'undefined') {
            if (whereStmt != "") {
                whereStmt += " AND DeletedOn is null";
            }
            else {
                whereStmt += " DeletedOn is null";

            }
        }

        if (whereStmt != "") {
            return " WHERE " + whereStmt;
        }
        return "";
    }

    this.getOrderByStatement = function (tmpObject) {

        if (tmpObject.field && tmpObject.direction) {
            if (this.TableStructure[tmpObject.field].datatype == "datetime") {
                return " order by ISNULL(" + tmpObject.field + ",'2079-06-05T23:59:00') " + tmpObject.direction;

            }
            else {
                return " order by " + tmpObject.field + " " + tmpObject.direction;
            }
        }


        return "";
    }

    var ParseDataTypes = function (Field) {

        if (Field.datatype) {
            return Field.datatype;
        }

        try {
            var dt = Field.type.declaration;
            switch (dt) {
                case "nvarchar":
                    return "string";
            }
        }
        catch (ex) {
            console.log(ex);
        }

        return dt;
    }

    this.GetDataStructure = function () {
        var struct = {};

        struct.Fields = this.TableStructure;
        struct.NixObjectType = this.NixObjectType;

        struct.TableName = this.TableName;
        struct.BlankObject = {
            NixObjectType: this.NixObjectType
        }

        var columns = this.GetColumnNames();
        for (var i = 0; i < columns.length; i++) {
            struct.BlankObject[columns[i]] = null;
            struct.Fields[columns[i]].datatype = ParseDataTypes(struct.Fields[columns[i]]);
        }
        ;

        return struct;
    }

    this.GetIdentityValue = function (tmpObject) {
        var fieldObjs = Object.keys(this.TableStructure);
        for (var i = 0; i < fieldObjs.length; i++) {
            var field = this.TableStructure[fieldObjs[i]];
            if (field.identity) {
                return tmpObject[fieldObjs[i]];
            }
        }

        return null;
    }

    this.GetAllObjectValues = function (tmpObject) {
        var fieldObjs = Object.keys(this.TableStructure);
        var valueObj = {};
        for (var i = 0; i < fieldObjs.length; i++) {
            var field = this.TableStructure[fieldObjs[i]];

            if (field.identity == false && tmpObject[field.name] !== undefined) {
                valueObj[field.name] = tmpObject[field.name];
            }
        }
        ;

        return valueObj;
    }

    this.GetReferenceRequests = function (dataLoadOptions, data) {

        var currentLoadOptions = dataLoadOptions[this.TableName.toLowerCase()];

        var referencesToLoad = [];
        var dcs = [];

        for (var i = 0; i < currentLoadOptions.length; i++) {
            var field = currentLoadOptions[i];
            if (this.TableStructure[field].datatype == Nix.DataTypes.Reference_Array || this.TableStructure[field].datatype == Nix.DataTypes.Reference_Object) {
                referencesToLoad.push(field);
            }
        }

        if (referencesToLoad.length == 0)
            return [];


        for (var i = 0; i < referencesToLoad.length; i++) {
            var ref = referencesToLoad[i];
            var field = this.TableStructure[ref];

            var tmpValues = [];
            for (var j = 0; j < data.length; j++) {
                var tmpObj = data[j];
                if (tmpValues.indexOf(tmpObj[field.field]) == -1) {
                    tmpValues.push(tmpObj[field.field]);
                }
            }

            var tmpDC = Nix.DalManager.GetBlankDataContext(field.model);

            tmpDC.Filters = {
                AND: {}
            };

            tmpDC.Filters.AND[field.fk_field] = {
                Operator: 'IN',
                Value: tmpValues
            };

            tmpDC.DataLoadOptions = dataLoadOptions;

            dcs.push(tmpDC);
        }

        return dcs;
    }.bind(this);

    this.SetReferences = function (data, dataLoadOptions, dataContextes) {

        var currentLoadOptions = dataLoadOptions[this.TableName.toLowerCase()];

        var dcDic = {};
        for (var i = 0; i < dataContextes.length; i++) {
            var tmpDC = dataContextes[i];
            dcDic[tmpDC.NixObjectType] = tmpDC.data;
        }


        var referencesToLoad = [];
        var dcs = [];

        for (var i = 0; i < currentLoadOptions.length; i++) {
            var field = currentLoadOptions[i];
            if (this.TableStructure[field].datatype == Nix.DataTypes.Reference_Array || this.TableStructure[field].datatype == Nix.DataTypes.Reference_Object) {
                referencesToLoad.push(field);
            }
        }

        if (referencesToLoad.length == 0)
            return [];


        for (var i = 0; i < referencesToLoad.length; i++) {
            var ref = referencesToLoad[i];
            var field = this.TableStructure[ref];

            var tmpValues = [];
            for (var j = 0; j < data.length; j++) {
                var tmpObj = data[j];

                var tmpList = dcDic[field.model];
                for (var k = 0; k < tmpList.length; k++) {
                    if (tmpList[k][field.fk_field] == tmpObj[field.field]) {
                        if (field.datatype == Nix.DataTypes.Reference_Array) {
                            if (Array.isArray(tmpObj[field.name]) == false) {
                                tmpObj[field.name] = [];
                            }

                            tmpObj[field.name].push(tmpList[k]);
                        }
                        else {
                            tmpObj[field.name] = tmpList[k];
                            break;
                        }
                    }
                }
            }

            for (var j = 0; j < data.length; j++) {
                var tmpObj = data[j];

                if (field.datatype == Nix.DataTypes.Reference_Array) {
                    if (typeof(tmpObj[field.name]) == "undefined") {
                        tmpObj[field.name] = [];
                    }
                }

            }
        }

    }.bind(this);
}