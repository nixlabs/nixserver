"use strict";
var sql = require('mssql');

module.exports = function(){
    this.ModuleName= "NixSqlConnector";
    this.ModulePriority = -89;

    var Nix = global.Nix;

    var dbConfig = Nix.Config.dbConfig;

    // SQL Server Configuration
    var config = {
        user: 'nix',
        server: 'NIX3',
        password: 'nix',
        database: 'GlassDesign',
        options: {
            encrypt: true // Use this if you're on Windows Azure
        }
    };

    Object.assign(config, dbConfig);

    var requestObject = function(query){
        this.Query = query;
        this.Success = false;
        this.Error = false; // store msg or err obj
        this.Executed = false;

        this.DataObj = null;
        this.DataList = null;

        this.SetData = function(data){

            this.Success = true;
            this.Executed = true;

            if(Array.isArray(data)){
                this.DataObj = data[0];
                this.DataList = data;
            }
            else{
                this.DataObj = data;
                this.DataList = [data];
            }
        }

        this.SetError = function(err){
            this.Error = err;
            this.Success = false;
            this.Executed = true;
        }
    };

    this.getRequestObject = function(query){
        return new requestObject(query);
    };

    this.Init = function(){

    };

    this.MakeDatabaseConnection = function (callBack) {
        var tmpConf = JSON.parse(JSON.stringify(config));

        //console.log("Creating connection: " + JSON.stringify(config));
        var connection = new sql.Connection(config, function (err) {
            if (err) {
                console.error("Error connection: " + JSON.stringify(err));
                connection.close();
                callBack(err);
            }
            else {
                console.log("Connection created");
                callBack(err, connection);
            }
        });

        connection.on('error', function (err) {
            connection.close();
            callBack(err);
        });
    };

    this.MakeDatabaseQuery = function (connection, query, callBack, closeConnection) {
        if (closeConnection == undefined)
            closeConnection = true;

        var request = new sql.Request(connection);

        console.log(query);

        request.query(query, function (err, recordsets, returnValue) {
            if (err) {
                connection.close();
                callBack(err);
            }

            if (closeConnection)
                connection.close();

            callBack(err, recordsets, returnValue);
        });
    };

    var MakeDatabaseQuery = this.MakeDatabaseQuery;
    var MakeDatabaseConnection = this.MakeDatabaseConnection;

    this.ExecuteQueries = function(requestObjects, callBack){

        if(Array.isArray(requestObjects) == false){
            requestObjects = [requestObjects];
        }

        for (var i = 0; i < requestObjects.length; i++) {
            var tmpObj = requestObjects[i];
            if((tmpObj instanceof requestObject) == false){
                throw "Not an NixSqlConnector.requestObject type. Use NixSqlConnector.GetRequestObject function to create one.";
            }
        }


        MakeDatabaseConnection(function (err, connection) {
            // ... error checks
            if (err) {
                requestObjects[0].SetError(err);
                callBack();
                return;
            }

            var counter = 0;

            var makeRequest = function () {
                MakeDatabaseQuery(connection, requestObjects[counter].Query, function (err, recordsets, returnValue) {
                    if (err) {
                        connection.close();
                        requestObjects[counter].SetError(err);
                        callBack();
                        return;
                    }

                    requestObjects[counter].SetData(recordsets);

                    counter++;
                    if (counter >= requestObjects.length) {
                        connection.close();
                        callBack();
                    } else {
                        makeRequest();
                    }
                }, false);
            };

            makeRequest();

        });
    };

};



