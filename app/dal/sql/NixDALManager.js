"use strict";

var sql = require('mssql');
var uuid = require('node-uuid');
module.exports = function () {
    this.ModuleName = "NixDALManager";
    this.ModulePriority = -87;

    var Nix = global.Nix;
    var DalModels = Nix.DalModels;
    var AppRegister = global.Nix.AppRegister;
    var socketHelper = global.SocketHelper;
    var restifyHelper = global.RestifyHelper;

    var OnSubmitBefore = {};
    var OnSubmitAfter = {};
    var OnLoadBefore = {};
    var OnLoadAfter = {};

    this.CallEvent = function (func, params, disableHandlers, callBack) {
        if (disableHandlers || func == null) {
            callBack();
        } else {
            func(params, callBack);
        }
    };


    this.AddOnSubmitBeforeHandler = function (nixObjectType, handler) {
        if (OnSubmitBefore[nixObjectType] == undefined) {
            OnSubmitBefore[nixObjectType] = [];
        }

        OnSubmitBefore[nixObjectType].push(handler);
    }

    this.AddOnSubmitAfterHandler = function (nixObjectType, handler) {
        if (OnSubmitAfter[nixObjectType] == undefined) {
            OnSubmitAfter[nixObjectType] = [];
        }

        OnSubmitAfter[nixObjectType].push(handler);
    }

    var _fireEvent = function (handlerList, counter, data, promiseResolve, promiseReject) {
        if (counter >= handlerList.length) {
            promiseResolve();
            return;
        }

        handlerList[counter](data, function () {
            counter++;
            _fireEvent(handlerList, counter, data, promiseResolve, promiseReject);
        }, promiseReject);
    }

    var _prepFireEvent = function (eventList, counter, data, promiseResolve, promiseReject) {
        if (counter >= data.length) {
            promiseResolve();
            return;
        }

        if (eventList[data[counter].NixObjectType] == undefined) {
            counter++;
            _prepFireEvent(eventList, counter, data, promiseResolve, promiseReject);
            return;
        }
        else {
            _fireEvent(eventList[data[counter].NixObjectType], 0, data[counter], function () {
                counter++;
                _prepFireEvent(eventList, counter, data, promiseResolve, promiseReject);
            }, promiseReject);
        }
    }

    var FireOnSubmitBeforeEvent = function (disableListener, data, promiseResolve, promiseReject) {
        if (disableListener) {
            promiseResolve();
            return;
        }

        _prepFireEvent(OnSubmitBefore, 0, data, promiseResolve, promiseReject);
    }

    var FireOnSubmitAfterEvent = function (disableListener, data, promiseResolve, promiseReject) {
        if (disableListener) {
            promiseResolve();
            return;
        }

        _prepFireEvent(OnSubmitAfter, 0, data, promiseResolve, promiseReject);
    }

    this.Init = function () {

    };

    this.GetBlankDataContext = function (nixObjectType) {
        return {
            "ID": uuid.v1(),
            "NixObjectType": nixObjectType,
            "NixDataObject": {
                "NixObjectType": nixObjectType,
            },
            "Filters": {},
            "success": true,
            "DataLoadOptions": [],
            // addFilter: function (name, value, operator) {
            //     console.log("in");
            //     if (this.Filters.AND == undefined || this.Filters.OR == undefined) {
            //         this.Filters = {
            //             AND: {},
            //             OR: {}
            //         }
            //     }
            //
            //     if (name.name != undefined) {
            //         name = name.name;
            //     }
            //     if (operator == undefined) {
            //         operator = "AND";
            //     }
            //
            //     if (typeof value == "object" && typeof value != 'array') {
            //         if (value.Operator != undefined) {
            //             this.Filters[operator][name] = value;
            //         }
            //         else {
            //             if (this.NixDataObject.Fields[name].datatype == 'string' || this.NixDataObject.Fields[name].datatype == 'text') {
            //                 value.Operator = 'Like N';
            //             }
            //             else {
            //                 value.Operator = '=';
            //             }
            //             this.Filters[operator][name] = value;
            //         }
            //     }
            //     else {
            //         if (value == null) {
            //             this.Filters.AND[name] = {Value: 'NULL', Operator: 'is'};
            //         }
            //         else if (typeof value == 'array') {
            //             this.Filters[operator][name] = {OR: []};
            //             for (var i = 0; i < value.length; i++) {
            //                 var obj = value[i];
            //                 if (obj.Operator != undefined) {
            //                     this.Filters.AND[name].OR.push({Value: obj.Value, Operator: obj.Operator});
            //                 }
            //                 else {
            //                     if (this.NixDataObject.Fields[name].datatype == 'string' || this.NixDataObject.Fields[name].datatype == 'text') {
            //                         this.Filters.AND[name].OR.push({Value: obj, Operator: 'Like N'});
            //                     }
            //                     else {
            //                         this.Filters.AND[name].OR.push({Value: obj, Operator: '='});
            //                     }
            //                 }
            //             }
            //         }
            //         else {
            //             if (value.Operator != undefined) {
            //                 this.Filters.AND[name] = {Value: value.Value, Operator: value.Operator};
            //             }
            //             else {
            //                 if (this.NixDataObject.Fields[name].datatype == 'string' || this.NixDataObject.Fields[name].datatype == 'text') {
            //                     this.Filters.AND[name] = {Value: value, Operator: 'Like N'};
            //                 }
            //                 else {
            //                     this.Filters.AND[name] = {Value: value, Operator: '='};
            //                 }
            //             }
            //
            //         }
            //     }
            // }
        }
    };

    this.GetObjectModel = function (ObjectData) {
        if (ObjectData.NixObjectType == undefined) {
            return null;
        }

        return DalModels.getModel(ObjectData.NixObjectType.toLowerCase());
    };

    this.GetModelList = function () {
        var modelList = [];
        for (var i = 0; i < DalModels.length; i++) {
            modelList.push({Name: DalModels[i].NixObjectType, Title: DalModels[i].TableName});
        }

        return modelList;
    };

    this.SetResponseError = function (requestObject, err, callBack) {
        requestObject.error = err;
        requestObject.false = true;
        callBack(err);
    };

    socketHelper.AddService('get:all', function (dataRequest, fn) {
        if (Array.isArray(dataRequest)) {
            var loadCounter = dataRequest.length;
            for (var i = 0; i < loadCounter; i++) {
                this.GetAllObjects(dataRequest[i], function (res) {
                    loadCounter--;
                    if (loadCounter == 0)
                        fn(dataRequest);
                });
            }
            ;
        }
        else {
            this.GetAllObjects(dataRequest, function (res) {
                fn(res);
            });
        }
    }.bind(this));

    restifyHelper.AddPostService("/nixdata", function (req, res, next) {
        var dataRequest = req.body;

        if (Array.isArray(dataRequest)) {
            var loadCounter = dataRequest.length;
            for (var i = 0; i < loadCounter; i++) {
                this.GetAllObjects(dataRequest[i], function (results) {
                    loadCounter--;
                    if (loadCounter == 0) {
                        restifyHelper.RespondSuccess(res, dataRequest);
                        next();
                    }
                }.bind(this));
            }
            ;
        }
        else {
            this.GetAllObjects(dataRequest, function (results) {
                restifyHelper.RespondSuccess(res, results);
                next();
            }.bind(this));
        }
    }.bind(this));

    socketHelper.AddService('get:submit', function (nixObjects, fn, user) {
        this.SubmitNixObjects_Transaction(nixObjects, user.ID, function (res) {
            fn(res);
        });
    }.bind(this));

    restifyHelper.AddPostService("/nixdatasubmit", function (req, res, next) {
        this.SubmitNixObjects_Transaction(req.body, req.user.ID, function (results) {
            restifyHelper.RespondSuccess(res, results);
            next();
        }.bind(this));
    }.bind(this));

    restifyHelper.AddPostService("/submitpotentialcustomer", function (req, res, next) {

        var Customer = null;

        if (req.body[0].Status != undefined) {
            if (req.body[0].Status == Nix.AppEnum.PotentialCustomerStatus.Accepted) {
                var PotentialCustomerDC = this.GetBlankDataContext(Nix.DAL.PotentialCustomers.NixObjectType);
                PotentialCustomerDC.Filters[Nix.DAL.PotentialCustomers.Fields.ID.name] = req.body[0].ID;
                var PotentialCustomersContactsDC = this.GetBlankDataContext(Nix.DAL.CustomerContacts.NixObjectType);
                PotentialCustomersContactsDC.Filters[Nix.DAL.CustomerContacts.Fields.PotentialCustomerID.name] = req.body[0].ID;
                var PotentialCustomersOffers = this.GetBlankDataContext(Nix.DAL.Offers.NixObjectType);
                PotentialCustomersOffers.Filters[Nix.DAL.Offers.Fields.PotentialCustomerID.name] = req.body[0].ID;
                var Company = this.GetBlankDataContext(Nix.DAL.Companies.NixObjectType);
                Company.Filters[Nix.DAL.Companies.Fields.ID.name] = req.body[0].CompanyID;

                var PotentialC = {};
                var PotentialCContacts = {};
                var PotentialCOffers = {};
                var Customers = {};
                var Comapnies = {};
                var dalManager = this;
                this.GetAllDataContextes([PotentialCustomerDC, PotentialCustomersContactsDC, PotentialCustomersOffers,Company], function (ress) {
                    PotentialC = PotentialCustomerDC.data[0];
                    PotentialCContacts = PotentialCustomersContactsDC.data;
                    PotentialCOffers = PotentialCustomersOffers.data;
                    Comapnies = Company.data[0];

                    if (PotentialC.CustomerID != null) {
                        Customer = dalManager.GetObjectModel({NixObjectType: Nix.DAL.Customers.NixObjectType});
                        Customer.ID = PotentialC.CustomerID;
                    }
                    else {
                        Customer = dalManager.GetObjectModel({NixObjectType: Nix.DAL.Customers.NixObjectType}).CreateNewObject();
                    }
                    Customer.Name = PotentialC.Name;
                    Customer[Nix.DAL.Customers.Fields.CompanyID.name] = PotentialC.CompanyID;
                    Customer[Nix.DAL.Customers.Fields.Address.name] = Comapnies.Sediste;
                    //Customer[Nix.DAL.Customers.Fields.Country.name] = Comapnies.Sediste;

                    Customer[Nix.DAL.Customers.Fields.PotentialCustomerID.name] = PotentialC.ID

                    dalManager.SubmitNixObjects_Transaction([Customer], req.user.ID, function (results) {
                        var returnedCustomer = results[0];
                        if (PotentialC.CustomerID != null) {
                            returnedCustomer.mergeObject.ID = PotentialC.CustomerID;
                        }
                        var tmpCompany = {
                            NixObjectID: "124",
                            NixObjectState: Nix.Enum.NixDataObjectStates.Updated,
                            NixObjectType: Nix.DAL.Companies.NixObjectType
                        };
                        tmpCompany.CustomerID = returnedCustomer.mergeObject.ID;
                        tmpCompany.ID = PotentialC.CompanyID;
                        var ObjectsForSub = [];
                        ObjectsForSub.push(tmpCompany);
                        req.body[0].CustomerID = returnedCustomer.mergeObject.ID;
                        ObjectsForSub.push(req.body[0]);
                        for (var i = 0; i < PotentialCContacts.length; i++) {
                            var obj = PotentialCContacts[i];
                            var tmpObj = {
                                NixObjectID: "124",
                                NixObjectState: Nix.Enum.NixDataObjectStates.Updated,
                                NixObjectType: Nix.DAL.CustomerContacts.NixObjectType
                            };
                            tmpObj.CustomerID = returnedCustomer.mergeObject.ID;
                            tmpObj.ID = obj.ID;
                            ObjectsForSub.push(tmpObj);
                        }

                        for (var i = 0; i < PotentialCOffers.length; i++) {
                            var obj = PotentialCOffers[i];
                            var tmpObj = {
                                NixObjectID: "124",
                                NixObjectState: Nix.Enum.NixDataObjectStates.Updated,
                                NixObjectType: Nix.DAL.Offers.NixObjectType
                            };
                            tmpObj.ID = obj.ID;
                            tmpObj.CompanyID = returnedCustomer.mergeObject.ID;
                            ObjectsForSub.push(tmpObj);
                        }

                        dalManager.SubmitNixObjects_Transaction(ObjectsForSub, req.user.ID, function (results) {
                            restifyHelper.RespondSuccess(ress, req.body);
                        }.bind(this));
                        restifyHelper.RespondSuccess(res, req.body);

                    }.bind(this));

                });
            }
        }

        if (Customer == null) {
            this.SubmitNixObjects_Transaction(req.body, req.user.ID, function (result) {
                restifyHelper.RespondSuccess(res, result);
            });
        }
    }.bind(this));

    /* Get objects */
    this.GetAllObjects = function (requestObject, callBack, disableListeners) {
        if (disableListeners == undefined) {
            disableListeners = false;
        }

        this.CallEvent(this.OnLoadBefore, requestObject, disableListeners, function (err) {
            if (err) {
                this.SetResponseError(requestObject, context.error, callBack);
            }

            var model = this.GetObjectModel({NixObjectType: requestObject.NixObjectType});

            if(Array.isArray(requestObject.DataLoadOptions)){
                var tmpLO = requestObject.DataLoadOptions;
                requestObject.DataLoadOptions = {};
                requestObject.DataLoadOptions[requestObject.NixObjectType] = tmpLO;
            }

            if(requestObject.DataLoadOptions[requestObject.NixObjectType.toLowerCase()] == undefined){
                requestObject.DataLoadOptions[requestObject.NixObjectType.toLowerCase()] = [];
            }


            var selectQuery = model.CreateSelectStatement(requestObject.DataLoadOptions[requestObject.NixObjectType], requestObject.Filters, requestObject.OrderBy, requestObject.Paging);

            var totalQuery = null;

            if (selectQuery.indexOf(";") > -1) {
                var tmpQueries = selectQuery.split(';');
                selectQuery = tmpQueries[0];
                totalQuery = tmpQueries[1];
            }

            var sqlRO = Nix.SqlConnector.getRequestObject(selectQuery);
            if (totalQuery != null) {
                sqlRO = [sqlRO];
                sqlRO.push(Nix.SqlConnector.getRequestObject(totalQuery));
            }

            Nix.SqlConnector.ExecuteQueries(sqlRO, function (a, b, c, d) {
                if (sqlRO.Error) {
                    this.SetResponseError(requestObject, sqlRO.Error, callBack);
                    return;
                }

                requestObject.success = true;

                if (Array.isArray(sqlRO)) {
                    requestObject.data = sqlRO[0].DataList;
                    requestObject.TotalItems = sqlRO[1].DataObj.TotalItems;
                }
                else {
                    requestObject.data = sqlRO.DataList;
                }

                var newDcs = model.GetReferenceRequests(requestObject.DataLoadOptions, requestObject.data);

                var finishRequest = function(){
                    this.CallEvent(this.OnLoadAfter, sqlRO.DataList, disableListeners, function (err) {
                         if (err) {
                            this.SetResponseError(requestObject, err, callBack);
                            return;
                        }

                        callBack(sqlRO.DataObj);
                    }.bind(this));
                }.bind(this);

                if(newDcs.length > 0){
                    this.GetAllDataContextes(newDcs, function(){
                        model.SetReferences(requestObject.data, requestObject.DataLoadOptions, newDcs);
                        finishRequest();
                    }.bind(this));
                }
                else{
                    finishRequest();
                }



            }.bind(this));

        }.bind(this));
    };

    this.GetAllDataContextes = function (dataContextes, callBack) {
        var loadCounter = dataContextes.length;

        for (var i = 0; i < loadCounter; i++) {
            this.GetAllObjects(dataContextes[i], function (res) {
                loadCounter--;
                if (loadCounter == 0)
                    callBack(dataContextes);
            });
        }
    }.bind(this);


    /* Submit objects */
    this.submitNextNixObject_Transaction = function (nixObjects, userID, objectIndex, nixResults, callBack, transaction) {

        var ObjectData = nixObjects[objectIndex];
        var NixObjectType = ObjectData.NixObjectType;
        var model = this.GetObjectModel({NixObjectType: NixObjectType});

        var rolledBack = false;

        var Query = "";

        switch (ObjectData.NixObjectState) {
            case Nix.Enum.NixDataObjectStates.New:
                Query = model.CreateInsertStatement(ObjectData);
                break;
            case Nix.Enum.NixDataObjectStates.Updated:
                Query = model.CreateUpdateStatement(ObjectData);
                break;
            case Nix.Enum.NixDataObjectStates.Deleted:
                Query = model.CreateDeleteStatement(ObjectData);
                // Query = model.CreateUpdateStatement(ObjectData);
                break;
            default:
                //log error
                break;
        }

        var request = new sql.Request(transaction);
        console.log(Query);
        request.query(Query, function (err, recordsets) {
            // insert should fail because of invalid value

            objectIndex++;

            var res = {
                NixObjectType: ObjectData.NixObjectType,
                NixObjectID: ObjectData.NixObjectID,
                Success: false,
                NixObjectState: ObjectData.NixObjectState,
                NixObjectExpired: false,
                mergeObject: {}
            };

            if (err) {
                res.Success = false;
                res.Error = err;
                res.NixObject = ObjectData;
            }
            else {
                res.Success = true;

                var objectId = model.GetIdentityValue(ObjectData);
                var objectData = model.GetAllObjectValues(ObjectData);

                if (ObjectData.NixObjectState == Nix.Enum.NixDataObjectStates.New) {
                    res.mergeObject.ID = recordsets[0].insertedID;
                    objectId = res.mergeObject.ID;
                }
                else if (ObjectData.NixObjectState == Nix.Enum.NixDataObjectStates.Deleted) {
                    res.mergeObject.NixObjectExpired = true;
                }

                AppRegister.LogObjectSubmit(ObjectData.NixObjectType, ObjectData.NixObjectState, objectId, objectData, userID);
            }

            nixResults.push(res);
            if (objectIndex < nixObjects.length && res.Success) {
                this.submitNextNixObject_Transaction(nixObjects, userID, objectIndex, nixResults, callBack, transaction);
            }
            else {
                if (err) {
                    if (!rolledBack) {
                        transaction.rollback(function (err) {
                            if (err) {
                                console.error("Error on RollBack:" + JSON.stringify(err));
                            }

                            callBack(nixResults);
                        });
                    }
                } else {
                    transaction.commit(function (err) {
                        if (err) {
                            console.error("Error on Commit:" + JSON.stringify(err));
                        }

                        callBack(nixResults);
                    });
                }
            }

        }.bind(this));
    };

    this.SubmitNixObjects_Transaction = function (nixObjects, _userID, _callBack, disableListeners) {

        if (disableListeners == undefined) {
            disableListeners = false;
        }

        var callBack = _callBack;
        var userID = _userID;

        if (_callBack == undefined && (typeof _userID) == "function") {
            userID = null;
            callBack = _userID;
        }

        FireOnSubmitBeforeEvent(disableListeners, nixObjects, function () {
            Nix.SqlConnector.MakeDatabaseConnection((function (err, connection) {
                // ... error checks
                if (err) {
                    return;
                }
                var transaction = new sql.Transaction(connection);
                transaction.begin(function (err) {
                    // ... error checks

                    var rolledBack = false;

                    transaction.on('rollback', function (aborted) {
                        // emited with aborted === true

                        rolledBack = true;
                    });

                    this.submitNextNixObject_Transaction(nixObjects, userID, 0, [], function (results) {
                        FireOnSubmitAfterEvent(disableListeners, nixObjects, function () {
                            callBack(results);
                        }.bind(this), callBack)
                    }.bind(this), transaction);

                }.bind(this));

            }).bind(this));
        }.bind(this), callBack);
    };
}



