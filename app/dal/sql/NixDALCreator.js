"use strict";
var fs = require('fs');
var jsonfile = require('jsonfile');

var DalObject = require('./NixDataObject.js');

module.exports = function () {
    this.ModuleName = "NixDALCreator";
    this.ModulePriority = -88;

    var Nix = global.Nix;

    var pathOutputFolder = Nix.Config.AppPath + "/structures";
    var pathDataStructure = pathOutputFolder + "/datastructure.js";
    var pathClientDataStrucure = pathOutputFolder + "/NixDataStructures.js";

    var pathClientClientDataStructure = Nix.Config.NixDataStructurePath;

    var Nix = global.Nix;
    var DalModels = Nix.DalModels = [];
    var DalModelsDic = {};


    DalModels.getModel = function (NixObjectType) {
        if (DalModelsDic[NixObjectType] != undefined)
            return DalModelsDic[NixObjectType];

        return null;
    }.bind(this);

    this.Init = function () {
        //buildDalObject(callBack);
        var promise = new Promise(checkDbStructure);
        return promise;
    };


    var checkDbStructure = function (fulfill, reject) {
        var tablesRO = Nix.SqlConnector.getRequestObject("SELECT * FROM information_schema.tables");
        Nix.SqlConnector.ExecuteQueries(tablesRO, function () {
            if (tablesRO.Success) {
                for (var i = 0; i < tablesRO.DataList.length; i++) {
                    var tmpTable = tablesRO.DataList[i];
                    var tmpDalObj = new DalObject(tmpTable.TABLE_NAME);
                    DalModelsDic[tmpDalObj.NixObjectType] = tmpDalObj;
                    DalModels.push(tmpDalObj);
                }

                var promise = new Promise(buildDalObject);
                promise.then(fulfill, reject);
            }
            else {
                reject(tablesRO.Error);
            }
        });
    };

    var buildDalStructure = function (callBack) {
        var DalStructure = {};

        for (var i = 0; i < DalModels.length; i++) {
            var struct = DalModels[i].GetDataStructure();

            DalStructure[struct.TableName] = struct;
        }

        var NixAppStructure = {};
        var tmpNixAppModels = global.Nix.AppRegister.ModelList;
        for (var i = 0; i < tmpNixAppModels.length; i++) {
            var struct = tmpNixAppModels[i].GetDataStructure();

            NixAppStructure[struct.TableName] = struct;
        }

        Nix.DAL = DalStructure;
        var json = "var Nix = Nix || {};\nNix.DAL = " + JSON.stringify(DalStructure, null, 4) + ";\n\n";
        json += "Nix.AppDal = " + JSON.stringify(NixAppStructure, null, 4) + ";";

        var jsonNodeModule = json + "\n\n var module = module || {};\nmodule.exports = Nix;"

        fs.mkdir(pathOutputFolder, function (e) {
            fs.writeFile(pathClientDataStrucure, jsonNodeModule, function (err) {
                if (err) {
                    console.log(err);
                    callBack(err);
                    return;
                }

                console.log("NixDataStructures file saved!");

                if (pathClientClientDataStructure) {
                    fs.writeFile(pathClientClientDataStructure, json, function (err) {
                        if (err) {
                            console.log(err);
                        }
                        else {
                            console.log("NixDataStructures on client saved!");
                        }

                        callBack(err);
                    });
                }
                else {
                    callBack();
                }
            });
        });

    }

    var buildDalObject = function (fulfill, reject, a, b, c) {
        var ds = null;

        try {
            try {
                ds = jsonfile.readFileSync(pathDataStructure);
                var NixDAL = require("../../../." + pathClientDataStrucure);
                Nix.DAL = NixDAL.DAL;

            } catch (ex) {
                throw("Files not found.");
            }

            for (var i = 0; i < DalModels.length; i++) {
                if (ds[DalModels[i].TableName]) {
                    DalModels[i].SetTableStructure(ds[DalModels[i].TableName]);
                }
                else {
                    throw("Table definition not found for " + DalModels[i].TableName);
                }
            }

            console.log("DataStructure OK!");
            fulfill();
            return;
        } catch (e) {
            console.error("Data structure update required, reason:" + e);
        }

        console.log("Building new data structure!");

        ds = {};

        Nix.SqlConnector.MakeDatabaseConnection(function (err, connection) {
            // ... error checks
            if (err) {
                reject(err);
                return;
            }

            var counter = 0;


            var requestObjs = [];
            for (var i = 0; i < DalModels.length; i++) {
                DalModels[i].reqObj = Nix.SqlConnector.getRequestObject('select * from ' + DalModels[i].TableName + ' where 1 = 2;');
                DalModels[i].reqObjRef = Nix.SqlConnector.getRequestObject('EXEC sp_fkeys \'' + DalModels[i].TableName + '\';');
                requestObjs.push(DalModels[i].reqObj);
                requestObjs.push(DalModels[i].reqObjRef);
            }

            Nix.SqlConnector.ExecuteQueries(requestObjs, function () {

                var tmpModels = {};

                for (var i = 0; i < DalModels.length; i++) {
                    var dalModel = DalModels[i];
                    tmpModels[dalModel.TableName] = dalModel;
                    if (dalModel.reqObj.Success) {
                        dalModel.SetTableStructure(dalModel.reqObj.DataList.columns);
                        ds[dalModel.TableName] = dalModel.reqObj.DataList.columns;
                    }
                    else {
                        reject(dalModel.reqObj.Error);
                        return;
                    }
                }


                for (var i = 0; i < DalModels.length; i++) {
                    var dalModel = DalModels[i];

                    if (dalModel.reqObjRef.Success) {
                        for (var j = 0; j < dalModel.reqObjRef.DataList.length; j++) {
                            var tmpRef = dalModel.reqObjRef.DataList[j];
                            dalModel.CreateReferenceStructure(tmpRef, tmpModels[tmpRef.FKTABLE_NAME]);
                        }
                    }
                    else {
                        reject(dalModel.reqObjRef.Error);
                        return;
                    }
                }

                console.log("Objects loaded: " + DalModels.length)
                buildDalStructure(function () {
                    fs.mkdir(pathOutputFolder, function (e) {
                        jsonfile.writeFileSync(pathDataStructure, ds);
                        fulfill();
                    });
                });
            }.bind(this));
        });
    }


}



