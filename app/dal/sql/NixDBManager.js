"use strict";
var DalObject = require('./NixDataObject.js');
var fs = require('fs');
var jsonfile = require('jsonfile');

module.exports = function () {
    this.ModuleName = "NixDBManager";
    this.ModulePriority = -86;

    var restifyHelper = global.RestifyHelper;
    var dataStructurePath = global.Nix.Config.AppPath + "/structures/datastructure.js";
    var DBdataStructurePath = global.Nix.Config.AppPath + "/structures/datastructureDB.js";
    var downloadedDataPath = "./temp/structures/";


    restifyHelper.AddGetService("/db/status/", function (req, res, next) {
        var tablesRO = Nix.SqlConnector.getRequestObject("SELECT * FROM information_schema.tables");
        Nix.SqlConnector.ExecuteQueries(tablesRO, function () {
            if (tablesRO.Success)
                restifyHelper.RespondSuccess(res, {status: true});
            else
                restifyHelper.RespondSuccess(res, {status: false});
        });

        next();

    }.bind(this), false);

    restifyHelper.AddGetService("/db/drop/", function (req, res, next) {
        var ds;
        try {
            ds = jsonfile.readFileSync(dataStructurePath, "utf-8");
            var dropScript = "";
            var queueF = [];
            var queueL = [];
            for (var tableName in ds) {
                var flag = true;
                if (ds[tableName].EntityType == "table") {
                    for (var fields in ds[tableName].Fields) {
                        if (ds[tableName].Fields[fields].datatype == "reference_array") {
                            queueF.push(ds[tableName].Fields[fields].model);
                            flag = false;
                        }
                        if (ds[tableName].Fields[fields].datatype == "reference_object") {
                            queueL.push(ds[tableName].Fields[fields].model);
                            flag = false;
                        }
                    }
                }
                if (flag && ds[tableName].EntityType == "table")
                    dropScript += "DROP TABLE " + tableName + ";\n";
            }
            queueF = queueF.filter(function (elem, pos) {
                return queueF.indexOf(elem) == pos;
            });
            queueL = queueL.filter(function (elem, pos) {
                return queueL.indexOf(elem) == pos;
            });
            var loop = true;
            while (loop) {
                loop = false;
                for (var tableName in ds) {
                    var index = queueF.indexOf(tableName);
                    if (index > -1) {
                        for (var fields in ds[tableName].Fields) {
                            if (ds[tableName].Fields[fields].datatype == "reference_array") {
                                var tmpIndex = queueF.indexOf(ds[tableName].Fields[fields].model);
                                if (tmpIndex != -1 && index < tmpIndex) {
                                    loop = true;
                                    queueF.splice(tmpIndex, 1);
                                    queueF.splice(queueF.indexOf(tableName), 0, ds[tableName].Fields[fields].model);
                                }
                            }
                        }
                    }
                }
            }
            queueF = queueF.concat(queueL);
            queueF = queueF.filter(function (elem, pos) {
                return queueF.indexOf(elem) == pos;
            });
            for (var i in queueF) {
                dropScript += "DROP TABLE " + queueF[i] + ";\n";
            }
            var reqObj = Nix.SqlConnector.getRequestObject(dropScript);
            Nix.SqlConnector.ExecuteQueries(reqObj, function () {
                if (reqObj.Success)
                    restifyHelper.RespondSuccess(res, {success: true});
                else
                    restifyHelper.RespondSuccess(res, reqObj.Error);
            });
        }
        catch (ex) {
            throw (ex);
        }
        next();
    }.bind(this), false);

    restifyHelper.AddGetService("/db/getTableNames/", function (req, res, next) {
        var ds;
        var data = [];
        try {
            ds = jsonfile.readFileSync(dataStructurePath, "utf-8");
            for (var tableName in ds) {
                if (ds[tableName].EntityType == "table")
                    data.push(tableName);
            }
            restifyHelper.RespondSuccess(res, {success: true, data: data});
        }
        catch (ex) {
            restifyHelper.RespondSuccess(res, {success: false, error: ex});
        }
        next();

    }.bind(this), false);

    restifyHelper.AddGetService("/db/getColumns/:table_name", function (req, res, next) {
        try {
            var ds = jsonfile.readFileSync(dataStructurePath, "utf-8");
            restifyHelper.RespondSuccess(res, {success: true, data: ds[res.req.params.table_name].Fields});
        }
        catch (ex) {
            restifyHelper.RespondSuccess(res, {success: false, error: ex});
        }
        next();

    }.bind(this), false);

    restifyHelper.AddGetService("/db/newtable/:addData", function (req, res, next) {
        var newTableScript = "";
        var refScript = "";
        var textImageFlag = false;
        var foreignKeys = [];

        //      -------      CREATING TABLES AND FIELDS   ----------

        try {
            var ds = jsonfile.readFileSync(dataStructurePath, "utf-8");
            var tableNames = [];
            for (var tableName in ds) {
                if (ds[tableName].EntityType == "table") {
                    tableNames.push(tableName);
                    var tmpScript = "SET ANSI_NULLS ON SET QUOTED_IDENTIFIER ON CREATE TABLE [dbo].[" + tableName + "](";
                    for (var fields in ds[tableName].Fields) {
                        switch (ds[tableName].Fields[fields].datatype) {
                            case "int":
                                if (ds[tableName].Fields[fields].identity)
                                    tmpScript += "[" + fields + "] [int] IDENTITY(1,1) NOT NULL,";
                                else {
                                    tmpScript += "[" + fields + "] [int] ";
                                    if (ds[tableName].Fields[fields].nullable)
                                        tmpScript += "NULL,";
                                    else
                                        tmpScript += "NOT NULL,";
                                }
                                break;
                            case "string":
                                tmpScript += "[" + fields + "] [nvarchar](";
                                if (ds[tableName].Fields[fields].length == 65535) {
                                    textImageFlag = true;
                                    tmpScript += "max) ";
                                }
                                else
                                    tmpScript += ds[tableName].Fields[fields].length / 2 + ") ";
                                if (ds[tableName].Fields[fields].nullable)
                                    tmpScript += "NULL,";
                                else
                                    tmpScript += "NOT NULL,";
                                break;
                            case "decimal":
                                tmpScript += "[" + fields + "] [decimal](" + ds[tableName].Fields[fields].precision + "," + ds[tableName].Fields[fields].scale + ") ";
                                if (ds[tableName].Fields[fields].nullable)
                                    tmpScript += "NULL,";
                                else
                                    tmpScript += "NOT NULL,";
                                break;
                            case "datetime":
                                tmpScript += "[" + fields + "] [datetime] ";
                                if (ds[tableName].Fields[fields].nullable)
                                    tmpScript += "NULL,";
                                else
                                    tmpScript += "NOT NULL,";
                                break;
                            case "reference_object":
                                if (foreignKeys.indexOf("FK_" + tableName + "_" + ds[tableName].Fields[fields].model) == -1) {
                                    foreignKeys.push("FK_" + tableName + "_" + ds[tableName].Fields[fields].model);
                                    refScript += "ALTER TABLE [dbo].[" + tableName + "] WITH CHECK ADD  CONSTRAINT [FK_" + tableName;
                                    refScript += "_" + ds[tableName].Fields[fields].model + "] FOREIGN KEY([" + ds[tableName].Fields[fields].field + "])";
                                    refScript += "REFERENCES [dbo].[" + ds[tableName].Fields[fields].model + "] ([" + ds[tableName].Fields[fields].fk_field + "])";
                                    refScript += "ALTER TABLE [dbo].[" + tableName + "] CHECK CONSTRAINT [FK_" + tableName + "_" + ds[tableName].Fields[fields].model + "]";
                                }
                                else {
                                    var counter = 1;
                                    var fkName = "FK_" + tableName + "_" + ds[tableName].Fields[fields].model;
                                    var tmpFkName = fkName;
                                    while (foreignKeys.indexOf(tmpFkName) > -1) {
                                        fkName = "FK_" + tableName + "_" + ds[tableName].Fields[fields].model + counter;
                                        counter++;
                                        foreignKeys.push(fkName);
                                        tmpFkName = tmpFkName.substr(0, fkName.length - 1);
                                        tmpFkName += counter;
                                    }
                                    refScript += "ALTER TABLE [dbo].[" + tableName + "] WITH CHECK ADD  CONSTRAINT [" + fkName;
                                    refScript += "] FOREIGN KEY([" + ds[tableName].Fields[fields].field + "])";
                                    refScript += "REFERENCES [dbo].[" + ds[tableName].Fields[fields].model + "] ([" + ds[tableName].Fields[fields].fk_field + "])";
                                    refScript += "ALTER TABLE [dbo].[" + tableName + "] CHECK CONSTRAINT [" + fkName + "]";
                                }

                        }
                    }
                    tmpScript += " CONSTRAINT [PK_" + tableName + "] PRIMARY KEY CLUSTERED";
                    tmpScript += "([ID] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]";
                    tmpScript += ") ON [PRIMARY]";
                    if (textImageFlag)
                        tmpScript += " TEXTIMAGE_ON [PRIMARY]";
                    textImageFlag = false;
                    newTableScript += tmpScript;
                }
            }
            if (res.req.params.addData == "true") {
                var DataScript = addData(tableNames);
                newTableScript += DataScript;
            }
            newTableScript += refScript;
            var reqObj = Nix.SqlConnector.getRequestObject(newTableScript);
            Nix.SqlConnector.ExecuteQueries(reqObj, function () {
                if (reqObj.Success) {
                    try {
                        jsonfile.writeFileSync(DBdataStructurePath, ds);
                        restifyHelper.RespondSuccess(res, {success: true});
                    }
                    catch (ex) {
                        restifyHelper.RespondSuccess(res, {success: false, error: ex});
                    }
                }
                else
                    restifyHelper.RespondSuccess(res, {success: false, error: reqObj.Error});
            });
        }
        catch (ex) {
            restifyHelper.RespondSuccess(res, {success: false, error: ex});
        }
        next();
    }.bind(this), false);

    restifyHelper.AddGetService("/db/check/Tables/:table_name", function (req, res, next) {
        var table = res.req.params.table_name;
        if (table != "*") {
            var tablesRO = Nix.SqlConnector.getRequestObject("SELECT * FROM " + table);
            Nix.SqlConnector.ExecuteQueries(tablesRO, function () {
                if (tablesRO.Success)
                    restifyHelper.RespondSuccess(res, {exists: true});
                else
                    restifyHelper.RespondSuccess(res, {exists: false});
            });
        }
        else {
            var tablesRO = Nix.SqlConnector.getRequestObject("SELECT * FROM information_schema.tables");
            var tables = [];
            var currTables = [];
            var extras = [];
            var missing = [];
            var respondMsg = "";
            Nix.SqlConnector.ExecuteQueries(tablesRO, function () {
                if (tablesRO.Success) {
                    for (var i = 0; i < tablesRO.DataList.length; i++) {
                        var tmpTable = tablesRO.DataList[i];
                        tables.push(tmpTable.TABLE_NAME);
                    }
                    for (var i = 0; i < DalModels.length; i++) {
                        var model = DalModels[i];
                        currTables.push(model.TableName);
                        if (tables.indexOf(model.TableName) == -1) {
                            missing.push(model.TableName);
                        }
                    }
                    for (var i = 0; i < tables.length; i++) {
                        var tmpTable = tables[i];
                        if (currTables.indexOf(tmpTable) == -1)
                            extras.push(tmpTable);
                    }
                    if (extras.length > 0)
                        respondMsg += "Extra tables: " + extras + "\n";
                    if (missing.length > 0)
                        respondMsg += "Missing tables: " + missing + "\n";
                    if (missing.length <= 0 && extras.length <= 0)
                        var tableOK = true;
                    else
                        var tableOK = false;
                    restifyHelper.RespondSuccess(res, {extra: extras, missing: missing, tableOK: tableOK});
                }
            });
        }
        next();

    }.bind(this), false);

    restifyHelper.AddPostService("/db/updateTables/", function (req, res, next) {
        var data = req.body;
        var query = "";
        for (var i = 0; i < data.tableNames.length; i++) {
            var name = data.tableNames[i];
            for (var j = 0; j < data.columns[name].length; j++) {
                query += "update [" + data.dbName + "].[dbo].[" + name + "] set ";
                var tableData = data.columns[name][j];
                for (var columnData in tableData) {
                    if (columnData != "ID") {
                        query += columnData + "=N'" + tableData[columnData] + "',";
                    }
                    else {
                        query = query.substr(0, query.length - 1);
                        query += " where ID='" + tableData[columnData] + "' ";
                    }
                }
            }
        }
        var reqObj = Nix.SqlConnector.getRequestObject(query);
        Nix.SqlConnector.ExecuteQueries(reqObj, function () {
            if (reqObj.Success)
                restifyHelper.RespondSuccess(res, {success: true});
            else
                restifyHelper.RespondSuccess(res, reqObj);
        });
        next();
    }.bind(this), false);


    var addData = function (tableNames) {
        var finalScript = "";
        for (var i = 0; i < tableNames.length; i++) {
            var tableName = tableNames[i];
            try {
                var data = jsonfile.readFileSync(downloadedDataPath + tableName + ".json", "utf-8");
                var insertScript = "SET IDENTITY_INSERT [dbo].[" + tableName + "] ON ";
                for (var j = 0; j < data.length; j++) {
                    insertScript += "insert [dbo].[" + tableName + "] (";
                    var fields = "";
                    var values = "";
                    var dataRow = data[j];
                    for (var field in dataRow) {
                        fields += "[" + field + "], ";
                        if (dataRow[field] == null) {
                            values += "" + dataRow[field] + ", ";
                        }
                        else {
                            values += "N'" + dataRow[field] + "', ";
                        }
                    }
                    values = values.substr(0, values.length - 2);
                    fields = fields.substr(0, fields.length - 2);
                    insertScript = insertScript.concat(fields);
                    insertScript += ") values (";
                    insertScript = insertScript.concat(values);
                    insertScript += ") \n";

                }
                insertScript += "SET IDENTITY_INSERT [dbo].[" + tableName + "] OFF ";
                finalScript += insertScript;
            }
            catch (ex) {
                throw (ex);
            }
        }
        return finalScript;

    }
};



