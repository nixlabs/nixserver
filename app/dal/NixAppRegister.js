"use strict";
var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var DalObject = require('./mongo/NixDataObject.js');

module.exports = function () {
  this.ModuleName = "NixAppRegister";
  this.ModulePriority = -98;

  var restifyHelper = global.RestifyHelper;
  var mongoDBConnection = null;
  if (Nix.Config.mongoDB) {
    mongoDBConnection = Nix.Config.mongoDB;
  }
  else {
    mongoDBConnection = Nix.Config.dbConfig.server;
  }

  var Models = this.Models = {};
  var ModelList = this.ModelList = [];

  this.Init = function () {
    var promise = new Promise(this.CheckDB);
    return promise;
  };

  this.GetModelList = function () {
    var modelList = [];
    var models = Object.keys(Models);
    for (var i = 0; i < models.length; i++) {
      modelList.push({Name: models[i], Title: models[i]});
    }

    return modelList;
  };


  this.AddModel = function (name, schema, addID) {
    var newModel = new mongoose.Schema(schema);

    if (addID) {
      newModel.plugin(autoIncrement.plugin, {model: name, field: 'ID', startAt: 1});
      schema.ID = Number;
    }

    var tmpModel = mongoose.model(name, newModel);
    var tmpDalObj = new DalObject(name);
    this.Models[name] = tmpModel;
    ModelList.push(tmpDalObj);


    var columns = Object.keys(schema);
    var tableStruct = {};

    for (var i = 0; i < columns.length; i++) {
      var tmpColumn = columns[i];

      tableStruct[tmpColumn] = {
        caseSensitive: false,
        name: tmpColumn,
        nullable: true,
        readOnly: false,
        index: i,
        identity: false
      };

      if (tmpColumn == "ID") {
        tableStruct[tmpColumn].nullable = false;
        tableStruct[tmpColumn].readOnly = true;
        tableStruct[tmpColumn].identity = true;
      }

      if (schema[tmpColumn] == Number) {
        tableStruct[tmpColumn].datatype = "int";
      }
      else if (schema[tmpColumn] == String) {
        tableStruct[tmpColumn].datatype = "string";
      }
      else if (schema[tmpColumn] == Object) {
        tableStruct[tmpColumn].datatype = "object";
      }
      else if (schema[tmpColumn] == Boolean) {
        tableStruct[tmpColumn].datatype = "bit";
      }
      else if (schema[tmpColumn] == Date) {
        tableStruct[tmpColumn].datatype = "datetime";
      }
    }

    tmpDalObj.SetTableStructure(tableStruct, tmpModel);

  }.bind(this);


  this.CheckDB = function (promiseResolve, promiseReject) {
    try {
      var connection = mongoose.connect(mongoDBConnection, function (err) {
        if (err) {
          promiseReject("Failed to connect to MongoDB. " + err);
        }
      });

      autoIncrement.initialize(connection);

      this.AddModel('NixDBLog', {
        NixDataObject: String,
        UserID: Number,
        ExecutedOn: Date,
        DataIdentity: Number,
        DataObject: Object,
        Command: String
      }, true);

      this.AddModel('NixUser', {
        CreatedOn: Date,
        Username: String,
        Password: String,
        FullName: String,
        UserGroup: Number,
      }, true);

      this.AddModel('NixUserGroup', {
        CreatedOn: Date,
        Name: String
      }, true);


      var promise = new Promise(this.CheckUsers);
      promise.then(promiseResolve, promiseReject);

    } catch (ex) {
      promiseReject(ex);
    }
  }.bind(this);

  this.CheckUsers = function (promiseResolve, promiseReject) {

    var CheckAdmin = function (superAdminGroupID) {

      this.Models.NixUser.findOne({'Username': 'superadmin'}, function (err, superadmin) {
        if (err) return promiseReject(err);

        if (superadmin == null) {
          superadmin = new this.Models.NixUser({
            Username: 'superadmin',
            Password: 'superadmin',
            FullName: "Nix Admin",
            UserGroup: superAdminGroupID,
            CreatedOn: new Date()
          });

          superadmin.save(function (err) {
            if (err) {
              return promiseReject(err);
            }

            console.log('Super admin user created!');
            promiseResolve();
          });
        }
        else {
          promiseResolve();
        }
      }.bind(this));

    }.bind(this);

    this.Models.NixUserGroup.findOne({'Name': 'superadmin'}, function (err, superAdminGroup) {
      if (err)
        return promiseReject(err);
      if (superAdminGroup == null) {
        superAdminGroup = new this.Models.NixUserGroup({
          Name: 'superadmin'
        });

        superAdminGroup.save(function (err) {
          if (err) {
            return promiseReject(err);
          }

          console.log('Super admin group created!');
          CheckAdmin(superAdminGroup.ID);
        });
      }
      else {
        var m = this.Models.User;
        CheckAdmin(superAdminGroup.ID);
      }
    }.bind(this));

  }.bind(this);

  this.LogObjectSubmit = function (nixDataObject, nixDataObjectState, dataIdentity, dataObject, userID) {
    var newDBLog = new this.Models.NixDBLog({
      NixDataObject: nixDataObject,
      ExecutedOn: new Date(),
      DataIdentity: dataIdentity,
      DataObject: dataObject,
      Command: nixDataObjectState,
      UserID: userID || null
    });

    newDBLog.save(function (err) {
      if (err) throw err;
      console.log('New db log saved successfully!');
    });
  }

  restifyHelper.AddGetService('/logs/:objecttype/:objectid', function (req, res, next) {
    var objecttype = req.params.objecttype;
    var objectid = req.params.objectid;

    Models.NixDBLog.find({NixDataObject: objecttype, DataIdentity: objectid},function(err,results){
      if(err){
        restifyHelper.RespondError(res, 403, err);
      } else{
        restifyHelper.RespondSuccess(res, results);
      }
    });

  }, false);

}
