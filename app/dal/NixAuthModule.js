"use strict";

module.exports = function () {
    this.ModuleName = "NixAuthModule";
    this.ModulePriority = -78;
    var socketHelper = global.SocketHelper;
    var AppRegister = global.Nix.AppRegister;
    var Nix = global.Nix;

    var ServicesPermissions = {};
    var ModelsPermissions = {};

    this.Init = function () {
        var promiseOS = new Promise(this.CheckObjectsAndServices);
        return promiseOS;
    }

    this.AddSchemas = function () {
        AppRegister.AddModel("NixModelPermission", {
            ModelName: String,
            Title: String,
            Description: String,
            Group: String,
            Read: Array,
            Write: Array
        }, true);

        AppRegister.AddModel("NixServicePermission", {
            ServiceName: String,
            Title: String,
            Description: String,
            Group: String,
            Read: Array,
            Write: Array
        }, true);
    };

    socketHelper.AddService("models_get", function (data, fn) {
        AppRegister.Models.NixModelPermission.find({}, null, {sort: {ModelName: "ascending"}}, function (err, models) {

            fn(err, models);

        });
    });
    socketHelper.AddService("services_get", function (data, fn) {
        AppRegister.Models.NixServicePermission.find({}, null, {sort: {ServiceName: "ascending"}}, function (err, services) {

            fn(err, services);
        });
    });

    socketHelper.AddService("services_update", function (services, fn) {
        var updateModel = {};

        for (var i = 0; i < services.length; i++) {
            var curItem = services[i];
            AppRegister.Models.NixServicePermission.update(
                {ID: curItem.ID},
                curItem,
                {multi: false}, function (err) {
                    fn(err);
                    this.RefreshPermissionsAndUpdateUsers();
                }.bind(this));
        }
    }.bind(this));

    socketHelper.AddService("models_update", function (models, fn) {
        var updateModel = {};

        var counter = models.length;

        for (var i = 0; i < models.length; i++) {
            var curItem = models[i];
            AppRegister.Models.NixModelPermission.update(
                {ID: curItem.ID},
                curItem,
                {multi: false}, function (err) {
                    if(err){
                        console.error(err);
                    }

                    counter--;
                    if(counter == 0){
                        this.RefreshPermissionsAndUpdateUsers();
                    }
                }.bind(this));
        }

        fn();
    }.bind(this));

    this.CheckObjectsAndServices = function (promiseResolve, promiseReject) {

        this.AddSchemas();

        var restServices = global.RestifyHelper.GetServiceList();
        var socketServices = global.SocketHelper.GetServiceList();
        var allServices = restServices.concat(socketServices);

        var dalModelList = global.Nix.DalManager.GetModelList();
        var appModelList = global.Nix.AppRegister.GetModelList();
        var modelList = dalModelList.concat(appModelList);


        var CheckModels = function () {
            AppRegister.Models.NixModelPermission.find(function (err, models) {
                if (err) {
                    promiseReject(err);
                    return;
                }

                for (var i = 0; i < models.length; i++) {
                    var tmpIndex = -1;

                    var tmpItem = modelList.filter(function(value){
                        return models[i].ModelName == value.Name;
                    });

                    if(tmpItem.length > 0){
                        tmpItem = tmpItem[0];
                        tmpIndex = modelList.indexOf(tmpItem);
                    }

                    if (tmpIndex > -1) {
                        modelList.splice(tmpIndex, 1);
                    }
                }

                for (var i = 0; i < modelList.length; i++) {
                    var tmpModel = modelList[i];

                    var tmpModelPermission = new AppRegister.Models.NixModelPermission({
                        ModelName: tmpModel.Name,
                        Title: tmpModel.Title,
                        Group: tmpModel.Name,
                        Description: "",
                        Read: [1],
                        Write: [1]
                    });

                    tmpModelPermission.save();
                }
                this.LoadServicesAndModelPermissions();
                promiseResolve();
            }.bind(this));
        }.bind(this);

        var CheckServices = function () {
            AppRegister.Models.NixServicePermission.find(function (err, services) {
                if (err) {
                    promiseReject(err);
                    return;
                }

                for (var i = 0; i < services.length; i++) {
                    var tmpIndex = -1;

                    var tmpItem = allServices.filter(function(value){
                        return value.path == services[i].ServiceName
                    });

                    if(tmpItem.length > 0){
                        tmpItem = tmpItem[0];
                        tmpIndex = allServices.indexOf(tmpItem);
                    }

                    if (tmpIndex > -1) {
                        allServices.splice(tmpIndex, 1);
                    }
                }

                for (var i = 0; i < allServices.length; i++) {
                    var tmpService = allServices[i];

                    var tmpNewService = new AppRegister.Models.NixServicePermission({
                        ServiceName: tmpService.path,
                        Title: tmpService.title,
                        Description: tmpService.description,
                        Group: tmpService.group,
                        Read: [1],
                        Write: [1]
                    });

                    tmpNewService.save();
                }

                CheckModels();

            }.bind(this));
        }.bind(this);


        CheckServices();

        promiseResolve();
    }.bind(this);

    this.RefreshPermissionsAndUpdateUsers = function () {
        this.LoadServicesAndModelPermissions(function () {
            Nix.UserModule.UpdateAllActiveUsersPermissions();
            Nix.UserModule.UpdateAllActiveUsers();
        }.bind(this));
    }.bind(this);

    this.LoadServicesAndModelPermissions = function (callBack) {

        var ServicesLoaded = false;
        var ModelsLoaded = false;

        var FinishLoading = function() {
            if (ServicesLoaded && ModelsLoaded && callBack) {
                callBack();
            }
        }

        AppRegister.Models.NixServicePermission.find(function (err, services) {
            for (var i = 0; i < services.length; i++) {
                ServicesPermissions[services[i].ServiceName] = services[i]._doc;
            }

            ServicesLoaded = true;
            FinishLoading();
        });

        AppRegister.Models.NixModelPermission.find(function (err, models) {
            for (var i = 0; i < models.length; i++) {
                ModelsPermissions[models[i].ModelName] = models[i]._doc;
            }

            ModelsLoaded = true;
            FinishLoading();
        });
    };

    this.GetServicesAndModelsByList = function (UserGroupID) {
        var allowedServices = {};
        var services = Object.keys(ServicesPermissions);

        for (var i = 0; i < services.length; i++) {
            var servicePerm = ServicesPermissions[services[i]];
            if (servicePerm.Read.indexOf(UserGroupID) > -1) {
                allowedServices[servicePerm.ServiceName] = servicePerm.ID;
            }
        }

        var allowedModels = {};
        var models = Object.keys(ModelsPermissions);

        for (var i = 0; i < models.length; i++) {
            var modelPerm = ModelsPermissions[models[i]];
            if (modelPerm.Read.indexOf(UserGroupID) > -1) {
                allowedModels[modelPerm.ModelName] = modelPerm.ID;
            }
        }

        return {
            AllowedServices: allowedServices,
            AllowedModels: allowedModels
        };
    }.bind(this);
};