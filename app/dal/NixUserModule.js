"use strict";

module.exports = function () {
  this.ModuleName = "NixUserModule";
  this.ModulePriority = -79;

  var appRegister = global.Nix.AppRegister;
  var dalManager = global.Nix.DalManager;
  var socketHelper = global.SocketHelper;
  var restifyHelper = global.RestifyHelper;

  var Nix = global.Nix;

  var UsersDic = {};
  var UserGroupsDic = {};

  this.Init = function () {
    var promise = new Promise(LoadAllUsers);
    return promise;
  };

  var LoadAllUsers = function (promiseResolve, promiseReject) {
    appRegister.Models.NixUser.find(function (err, users) {
      if (err) {
        return promiseReject(err)
      }

      UsersDic = {};

      for (var i = 0; i < users.length; i++) {
        var user = users[i];
        UsersDic[user.ID] = user._doc;
        user.Password = "";
      }

      LoadAllUserGroups(promiseResolve, promiseReject);
    });
  };

  var LoadAllUserGroups = function (promiseResolve, promiseReject) {
    appRegister.Models.NixUserGroup.find(function (err, userGroups) {
      if (err) {
        return promiseReject(err)
      }

      UserGroupsDic = {};

      for (var i = 0; i < userGroups.length; i++) {
        UserGroupsDic[userGroups[i].ID] = userGroups[i];
      }
      promiseResolve();
    });
  };

  this.GetUserByID = function (userID) {
    if (UsersDic[userID]) {
      if (UsersDic[userID].Permissions == undefined || UsersDic[userID].Permissions == null) {
        UsersDic[userID].Permissions = Nix.AuthModule.GetServicesAndModelsByList(UsersDic[userID].UserGroup);
      }
      return UsersDic[userID];
    }

    return null;
  }.bind(this);

  this.GetUserList = function () {
    var ids = Object.keys(UsersDic);
    var userList = [];
    for (var i = 0; i < ids.length; i++) {
      userList.push(UsersDic[ids[i]]);
    }

    return userList;
  }.bind(this);

  this.GetUserGroupByID = function (userGroupID) {
    return UserGroupsDic[userGroupID];
  }.bind(this);

  this.GetUserGroupList = function () {
    var ids = Object.keys(UserGroupsDic);
    var userGroupList = [];
    for (var i = 0; i < ids.length; i++) {
      userGroupList.push(UserGroupsDic[ids[i]]);
    }

    return userGroupList;
  }.bind(this);

  this.UpdateAllActiveUsersPermissions = function () {
    var userIds = Object.keys(UsersDic);
    for (var i = 0; i < userIds.length; i++) {
      UsersDic[userIds[i]].Permissions = null;

    }
  }

  this.UpdateAllActiveUsers = function (promiseResolve, promiseReject) {
    var userIds = socketHelper.GetActiveUsers();
    for (var i = 0; i < userIds.length; i++) {
      var userID = userIds[i];
      var tmpUser = this.GetUserByID(userID);

      socketHelper.EmitToUser(tmpUser.ID, "userobject", tmpUser);
    }
  }

  this.UpdateUser = function (userID) {
    var tmpUser = this.GetUserByID(userID);
    socketHelper.EmitToUser(tmpUser.ID, "userobject", tmpUser);
  };

  restifyHelper.AddPostService("/authenticate", function (req, res, next) {
    appRegister.Models.NixUser.findOne({
      Username: req.body.Username,
      Password: req.body.Password
    }, function (err, user) {
      if (err) {
        restifyHelper.RespondError(res, 403, err);
      }
      else if (user == null) {
        restifyHelper.RespondError(res, 401, "Authentication failed!");
      }
      else {
        var resObj = {};
        resObj.User = this.GetUserByID(user.ID);
        //resObj.Permissions = Nix.AuthModule.GetServicesAndModelsByList(resObj.User.UserGroup);
        resObj.Token = restifyHelper.CreateToken(resObj.User);
        restifyHelper.RespondSuccess(res, resObj);
      }

      next();
    }.bind(this));
  }.bind(this), false);

  socketHelper.AddService("users_get", function (data, fn) {
    if (data.ID != undefined) {
      fn(null, this.GetUserByID(data.ID));
    }
    else {
      var users = this.GetUserList();
      fn(null, users);
    }
  }.bind(this));

  socketHelper.AddService("users_update", function (user, fn) {
    var updateModel = {};
    if (user.Username) {
      updateModel.Username = user.Username;
    }
    if (user.FullName) {
      updateModel.FullName = user.FullName;
    }
    if (user.Password) {
      updateModel.Password = user.Password;
    }
    if (user.UserGroup) {
      if (user.UserGroup.UserGroup) {
        updateModel.UserGroup = user.UserGroup;
      }
    }

    appRegister.Models.NixUser.update(
      {ID: user.ID}, updateModel, {multi: false}, function (err) {
        fn(err);
      });

    appRegister.Models.NixUser.findOneAndUpdate({ID: user.ID}, {$set: updateModel}, {new: true}, function (err, user) {
      if (err) {
        fn(err);
      }

      UsersDic[user.ID] = user._doc;
      user.Password = "";
    });
  }.bind(this));

  socketHelper.AddService("users_create", function (user, fn) {
    var newUser = new appRegister.Models.NixUser({
      Username: user.Username,
      Password: user.Password,
      FullName: user.FullName,
      UserGroup: user.UserGroup,
      CreatedOn: new Date()
    });

    newUser.save(function (err) {
      var promise = new Promise(LoadAllUsers);
      promise.then(function () {
        fn(newUser.ID)
      }, fn);
    });
  }.bind(this));

  socketHelper.AddService("users_delete", function (userID, fn) {
    appRegister.Models.NixUser.remove({'ID': userID}, function (err, res) {
      if (err){
        fn(false);
      }
      else {
        fn(true);
        var promise = new Promise(LoadAllUsers);
      }
    })
  }
    .bind(this));

  socketHelper.AddService("usergroups_get", function (data, fn) {
    if (data.ID != undefined) {
      fn(null, this.GetUserGroupByID(data.ID));
    }
    else {
      fn(null, this.GetUserGroupList());
    }
  }.bind(this));

  socketHelper.AddService("usergroups_create", function (usergroup, fn) {
    var newUserGroup = new appRegister.Models.NixUserGroup({
      Name: usergroup.Name
    });

    newUserGroup.save(function (err) {
      if (err) {
        fn(err);
        return;
      }

      var promise = new Promise(LoadAllUserGroups);
      promise.then(fn, fn);
    });
  }.bind(this));


};
