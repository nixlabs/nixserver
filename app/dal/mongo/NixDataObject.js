﻿var uuid = require('node-uuid');

module.exports = function (objectType, tableName) {
    tableName = tableName || objectType;

    this.NixObjectType = tableName.toLowerCase();

    this.TableName = tableName;

    this.TableStructure = null;

    var mongoModel = null;

    var Nix = global.Nix;

    this.ObjectDefinition = function (tableStructure) {
        var fields = Object.keys(tableStructure);
        for (var i = 0; i < fields.length; i++) {
            this[fields[i]] = null;
        }
        ;
    }

    this.columnNames = [];

    this.GetColumnNames = function () {
        if (this.columnNames.length == 0) {
            this.columnNames = Object.keys(this.TableStructure);
        }
        return this.columnNames;
    }

    this.SetTableStructure = function (tableStructure, model) {
        this.TableStructure = tableStructure;
        mongoModel = model;
    }

    this.GetMongoModel = function () {
        return mongoModel;
    }

    this.CreateNewObject = function () {
        var newObject = new this.ObjectDefinition(this.TableStructure);

        newObject.NixObjectID = uuid.v1();
        newObject.NixObjectState = Nix.Enum.NixDataObjectStates.New;
        newObject.NixObjectType = this.NixObjectType;

        return newObject;
    }.bind(this);

    this.ObjectSize = function (Object) {
        size = 0;
        for (key in Object) {
            if (Object.hasOwnProperty(key))
                size++;
        }
        return size;
    }

    this.CreateSelectStatement = function (DataLoadOptions, Filters, OrderBy) {
        var fields = "";

        if (DataLoadOptions) {
            var columns = this.GetColumnNames();

            for (var i = 0; i < columns.length; i++) {
                if (DataLoadOptions.indexOf(columns[i]) >= 0) {
                    if (fields.length > 0) {
                        fields += ", ";
                    }

                    fields += columns[i];
                }
            }
            ;
        }

        if (fields.length == 0)
            fields = "*";

        var whereStmt = "";

        if (this.ObjectSize(Filters) > 0) {
            whereStmt = this.getWhereStatement(Filters);
        }

        var orderByStmt = "";
        if (OrderBy && OrderBy != null) {
            orderByStmt = this.getOrderByStatement(OrderBy);
        }


        return "SELECT " + fields + " FROM " + this.TableName + whereStmt + orderByStmt;
    }

    this.CreateUpdateStatement = function (UpdateObject) {
        var result = {
            Filter: {},
            Data: {}
        };
        var fieldObjs = Object.keys(this.TableStructure);
        for (var i = 0; i < fieldObjs.length; i++) {
            var field = this.TableStructure[fieldObjs[i]];

            if (field.identity) {
                if (UpdateObject[field.name] != undefined) {
                    result.Filter[field.name] = UpdateObject[field.name];
                }
            }
            else {
                if (UpdateObject[field.name] !== undefined) {
                    result.Data[field.name] = UpdateObject[field.name];
                }
            }
        }

        return result;
    };

    this.CreateInsertStatement = function (tmpObject) {

        var result = {
            Filter: {},
            Data: {}
        };

        var fieldObjs = Object.keys(this.TableStructure);

        for (var i = 0; i < fieldObjs.length; i++) {
            var field = this.TableStructure[fieldObjs[i]];

            if (field.identity == false) {
                result.Data[field.name] = null;

                if (tmpObject[field.name] != null) {
                    result.Data[field.name] = tmpObject[field.name];
                }

            }

        }

        return result;
    }

    this.CreateDeleteStatement = function (tmpObject) {
        var result = {
            Filter: {},
            Data: {}
        };

        var fieldObjs = Object.keys(this.TableStructure);
        for (var i = 0; i < fieldObjs.length; i++) {
            var field = this.TableStructure[fieldObjs[i]];

            if (field.identity) {
                if (tmpObject[field.name] != undefined) {
                    result.Filter[field.name] = tmpObject[field.name];
                    break;
                }
            }
        }

        return result;
    }

    this.getWhereStatement = function (tmpObject) {
        if (tmpObject.AND == undefined && tmpObject.OR == undefined) {
            var whereObj = {$and: [{}]};

        }
        else {
            var whereObj = {$and: []};


            for (var filterField in tmpObject) {
                var Res = {};
                Res['$' + filterField.toLowerCase()] = [];

                for (var field in tmpObject[filterField]) {
                    var tmpRes = {};
                    if (tmpObject[filterField][field].Value == undefined) {

                        for (var nestedFilter in tmpObject[filterField][field]) {
                            var nestedFilterRes = {};
                            if (nestedFilter != "Operator") {
                                if (tmpRes['$' + nestedFilter.toLowerCase()] == undefined) {
                                    tmpRes['$' + nestedFilter.toLowerCase()] = [];
                                }
                                for (var nestedField in tmpObject[filterField][field][nestedFilter]) {
                                    var tmpObj = tmpObject[filterField][field][nestedFilter][nestedField]
                                    var pom = {};
                                    pom[field] = {}
                                    switch (tmpObj.Operator) {
                                        case '=':
                                            pom[field] = {$eq: tmpObj.Value};
                                            break;
                                        case '>':
                                            pom[field] = {$gt: tmpObj.Value};
                                            break;
                                        case '<':
                                            pom[field] = {$lt: tmpObj.Value};
                                            break;
                                        case '>=':
                                            pom[field] = {$gte: tmpObj.Value};
                                            break;
                                        case '<=':
                                            pom[field] = {$lte: tmpObj.Value};
                                            break;
                                        case '!=':
                                            pom[field] = {$ne: tmpObj.Value};
                                            break;
                                        case 'Like N':
                                            pom[field] = {'$regex': '(?i)(' + tmpObj.Value + ')'};
                                            break;
                                        case 'in':
                                            pom[field] = {$in: tmpObj.Value};
                                            break;
                                    }
                                    tmpRes['$' + nestedFilter.toLowerCase()].push(pom);
                                }
                            }
                        }

                    }
                    else {
                        tmpRes[field] = {};
                        switch (tmpObject[filterField][field].Operator) {
                            case '=':
                                tmpRes[field] = {$eq: tmpObject[filterField][field].Value};
                                break;
                            case '>':
                                tmpRes[field] = {$gt: tmpObject[filterField][field].Value};
                                break;
                            case '<':
                                tmpRes[field] = {$lt: tmpObject[filterField][field].Value};
                                break;
                            case '>=':
                                tmpRes[field] = {$gte: tmpObject[filterField][field].Value};
                                break;
                            case '<=':
                                tmpRes[field] = {$lte: tmpObject[filterField][field].Value};
                                break;
                            case '!=':
                                tmpRes[field] = {$ne: tmpObject[filterField][field].Value};
                                break;
                            case 'Like N':
                                tmpRes[field]= {'$regex': '(?i)(' + tmpObject[filterField][field].Value + ')'};
                                break;
                            case 'in':
                                tmpRes[field] = {$in: tmpObject[filterField][field].Value};
                                break;
                        }
                    }


                    Res['$' + filterField.toLowerCase()].push(tmpRes);
                }

                // {
                //     $and:[{
                //         Date:{$and:[{{$gte: '2015:01:01'}
                //             {{$lt: '2016:01:01'}]
                //     },
                //     ]
                // }
                //

                // AND:[
                //     Date:{
                //       AND:[{Value:'2015:01:01', Operator: '>='},
                //            {Value:'2016:01:01', Operator: '<'}
                //       ]
                // }]

                for (var resField in Res) {
                    if (Res[resField].length == 0) {
                        Res[resField].push({});
                    }
                }

                whereObj.$and.push(Res);
            }
        }
         return whereObj;
    }

    this.getOrderByStatement = function (tmpObject) {

        if (tmpObject.field && tmpObject.direction) {
            return " order by " + tmpObject.field + " " + tmpObject.direction;
        }

        return "";
    }

    var ParseDataTypes = function (Field) {

        try {
            var dt = Field.type.declaration;
            switch (dt) {
                case "nvarchar":
                    return "string";
            }
        }
        catch (ex) {
            console.log(ex);
        }

        return dt;
    }

    this.GetDataStructure = function () {
        var struct = {};

        struct.Fields = this.TableStructure;
        struct.NixObjectType = this.NixObjectType;

        struct.TableName = this.TableName;
        struct.BlankObject = {
            NixObjectType: this.NixObjectType
        }

        var columns = this.GetColumnNames();
        for (var i = 0; i < columns.length; i++) {
            struct.BlankObject[columns[i]] = null;
            //struct.Fields[columns[i]].datatype = ParseDataTypes(struct.Fields[columns[i]]);
        }
        ;

        return struct;
    }

    this.GetIdentityValue = function (tmpObject) {
        var fieldObjs = Object.keys(this.TableStructure);
        for (var i = 0; i < fieldObjs.length; i++) {
            var field = this.TableStructure[fieldObjs[i]];
            if (field.identity) {
                return tmpObject[fieldObjs[i]];
            }
        }

        return null;
    }

    this.GetAllObjectValues = function (tmpObject) {
        var fieldObjs = Object.keys(this.TableStructure);
        var valueObj = {};
        for (var i = 0; i < fieldObjs.length; i++) {
            var field = this.TableStructure[fieldObjs[i]];

            if (field.identity == false && tmpObject[field.name] !== undefined) {
                valueObj[field.name] = tmpObject[field.name];
            }
        }
        ;

        return valueObj;
    }
}