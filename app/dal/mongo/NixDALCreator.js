"use strict";
var fs = require('fs');
var jsonfile = require('jsonfile');

var Promise = require('promise');
var DalObject = require('./NixDataObject.js');


var Promise = require("promise");
var mongooseModule = require('mongoose');
var mongoose = new mongooseModule.Mongoose();
var autoIncrement = require('mongoose-auto-increment');

module.exports = function () {
  this.ModuleName = "NixDALCreator";
  this.ModulePriority = -89;

  var Nix = global.Nix;

  var pathOutputFolder = Nix.Config.AppPath + "/structures";
  var pathSchemasFolder = Nix.Config.AppPath + "/schemas";
  var pathDataStructure = pathOutputFolder + "/datastructure.js";
  var pathClientDataStrucure = pathOutputFolder + "/NixDataStructures.js";

  var pathClientClientDataStructure = Nix.Config.NixDataStructurePath;

  var Nix = global.Nix;
  var DalModels = Nix.DalModels = [];
  var DalModelsDic = {};


  var mongoDBConnection = null;

  if (Nix.Config.mongoDB) {
    mongoDBConnection = Nix.Config.mongoDB;
  }
  else {
    mongoDBConnection = Nix.Config.dbConfig.server;
  }


  DalModels.getModel = function (NixObjectType) {
    if (DalModelsDic[NixObjectType] != undefined)
      return DalModelsDic[NixObjectType];

    return null;
  }.bind(this);

  this.Init = function () {
    var promise = new Promise(this.CheckDB);
    return promise;
  }.bind(this);

  this.AddModel = function (name, schema, addID, startNumb) {
    var newModel = new mongoose.Schema(schema);
    var startNumber = 1;
    if (startNumb != undefined){
      startNumber = startNumb;
    }
    if (addID) {
      newModel.plugin(autoIncrement.plugin, {model: name, field: 'ID', startAt: startNumber});
      schema.ID = Number;
    }

    var tmpModel = mongoose.model(name, newModel);
    var tmpDalObj = new DalObject(name);
    DalModelsDic[tmpDalObj.NixObjectType] = tmpDalObj;
    DalModels.push(tmpDalObj);


    var columns = Object.keys(schema);
    var tableStruct = {};

    for (var i = 0; i < columns.length; i++) {
      var tmpColumn = columns[i];

      tableStruct[tmpColumn] = {
        caseSensitive: false,
        name: tmpColumn,
        nullable: true,
        readOnly: false,
        index: i,
        identity: false
      };

      if (tmpColumn == "ID") {
        tableStruct[tmpColumn].nullable = false;
        tableStruct[tmpColumn].readOnly = true;
        tableStruct[tmpColumn].identity = true;
      }

      if (schema[tmpColumn] == Number) {
        tableStruct[tmpColumn].datatype = "int";
      }
      else if (schema[tmpColumn] == String) {
        tableStruct[tmpColumn].datatype = "string";
      }
      else if (schema[tmpColumn] == Object) {
        tableStruct[tmpColumn].datatype = "object";
      }
      else if (schema[tmpColumn] == Boolean) {
        tableStruct[tmpColumn].datatype = "bit";
      }
      else if (schema[tmpColumn] == Date) {
        tableStruct[tmpColumn].datatype = "datetime";
      }
    }

    tmpDalObj.SetTableStructure(tableStruct, tmpModel);

  }.bind(this);

  var GetSchemaDescription = function (fields) {
    for (var field in fields) {
      if (fields[field] == "int") {
        fields[field] = Number;
      }
      else if (fields[field] == "string") {
        fields[field] = String;
      }
      else if (fields[field] == "object") {
        fields[field] = Object;
      }
      else if (fields[field] == "bit") {
        fields[field] = Boolean;
      }
      else if (fields[field] == "datetime") {
        fields[field] = Date;
      }


    }
    return fields;

  }

  this.CheckDB = function (promiseResolve, promiseReject) {
    try {
	    mongoose.Promise = global.Promise;
      var connection = mongoose.connect(mongoDBConnection, function (err) {
        if (err) {
          promiseReject("Failed to connect to MongoDB. " + err);
        }
      });

      autoIncrement.initialize(connection);

      var schemasMeta = {};

      var schemas = fs.readdirSync(pathSchemasFolder);
      for (var i = 0; i < schemas.length; i++) {
        var tmpSchema = jsonfile.readFileSync(pathSchemasFolder + "/" + schemas[i]);
        Object.assign(schemasMeta, tmpSchema);
      }

      var tmpObjects = Object.keys(schemasMeta);
      for (var i = 0; i < tmpObjects.length; i++) {
        var obj = schemasMeta[tmpObjects[i]];
        this.AddModel(tmpObjects[i], GetSchemaDescription(obj.Fields), obj.ID, obj.StartNumber)
      }

      var promise = new Promise(buildDalStructure);
      promise.then(promiseResolve, promiseReject);

    } catch (ex) {
      console.error(ex);
      promiseReject(ex);
    }
  }.bind(this);


  var buildDalStructure = function (callBack) {
    var DalStructure = {};

    for (var i = 0; i < DalModels.length; i++) {
      var struct = DalModels[i].GetDataStructure();

      DalStructure[struct.TableName] = struct;
    }
    ;

    var NixAppStructure = {};
    var tmpNixAppModels = global.Nix.AppRegister.ModelList;
    for (var i = 0; i < tmpNixAppModels.length; i++) {
      var struct = tmpNixAppModels[i].GetDataStructure();

      NixAppStructure[struct.TableName] = struct;
    }

    Nix.DAL = DalStructure;

    var json = "var Nix = Nix || {};\nNix.DAL = " + JSON.stringify(DalStructure, null, 4) + ";\n\n";
    json += "Nix.AppDal = " + JSON.stringify(NixAppStructure, null, 4) + ";";

    var jsonNodeModule = json + "\n\n var module = module || {};\nmodule.exports = Nix;"

    fs.mkdir(pathOutputFolder, function (e) {
      fs.writeFile(pathClientDataStrucure, jsonNodeModule, function (err) {
        if (err) {
          console.log(err);
          callBack(err);
          return;
        }

        console.log("NixDataStructures file saved!");

        if (pathClientClientDataStructure) {
          fs.writeFile(pathClientClientDataStructure, json, function (err) {
            if (err) {
              console.log(err);
            }
            else {
              console.log("NixDataStructures on client saved!");
            }

            callBack(err);
          });
        }
        else {
          callBack();
        }
      });
    });

  }


};



