"use strict";

var uuid = require('node-uuid');
var mongooseModule = require('mongoose');

module.exports = function () {
  this.ModuleName = "NixDALManager";
  this.ModulePriority = -88;

  var Nix = global.Nix;
  var DalModels = Nix.DalModels;
  var AppRegister = global.Nix.AppRegister;
  var socketHelper = global.SocketHelper;
  var restifyHelper = global.RestifyHelper;

  this.Models = {};

  var OnSubmitBefore = {};
  var OnSubmitAfter = {};
  var OnLoadBefore = {};
  var OnLoadAfter = {};

  var mongoose = new mongooseModule.Mongoose();

  var mongoDBConnection = null;
  if (Nix.Config.mongoDB) {
    mongoDBConnection = Nix.Config.mongoDB;
  }
  else {
    mongoDBConnection = Nix.Config.dbConfig.server;
  }

  this.CallEvent = function (func, params, disableHandlers, callBack) {
    if (disableHandlers || func == null) {
      callBack();
    } else {
      func(params, callBack);
    }
  };


  this.AddOnSubmitBeforeHandler = function (nixObjectType, handler) {
    if (OnSubmitBefore[nixObjectType] == undefined) {
      OnSubmitBefore[nixObjectType] = [];
    }

    OnSubmitBefore[nixObjectType].push(handler);
  }

  this.AddOnSubmitAfterHandler = function (nixObjectType, handler) {
    if (OnSubmitAfter[nixObjectType] == undefined) {
      OnSubmitAfter[nixObjectType] = [];
    }

    OnSubmitAfter[nixObjectType].push(handler);
  }

  var _fireEvent = function (handlerList, counter, data, promiseResolve, promiseReject) {
    if (counter >= handlerList.length) {
      promiseResolve();
      return;
    }

    handlerList[counter](data, function () {
      counter++;
      _fireEvent(handlerList, counter, data, promiseResolve, promiseReject);
    }, promiseReject);
  }

  var _prepFireEvent = function (eventList, counter, data, promiseResolve, promiseReject) {
    if (counter >= data.length) {
      promiseResolve();
      return;
    }

    if (eventList[data[counter].NixObjectType] == undefined) {
      counter++;
      _prepFireEvent(eventList, counter, data, promiseResolve, promiseReject);
      return;
    }
    else {
      _fireEvent(eventList[data[counter].NixObjectType], 0, data[counter], function () {
        counter++;
        _prepFireEvent(eventList, counter, data, promiseResolve, promiseReject);
      }, promiseReject);
    }
  }

  var FireOnSubmitBeforeEvent = function (disableListener, data, promiseResolve, promiseReject) {
    if (disableListener) {
      promiseResolve();
      return;
    }

    _prepFireEvent(OnSubmitBefore, 0, data, promiseResolve, promiseReject);
  }

  var FireOnSubmitAfterEvent = function (disableListener, data, promiseResolve, promiseReject) {
    if (disableListener) {
      promiseResolve();
      return;
    }

    _prepFireEvent(OnSubmitAfter, 0, data, promiseResolve, promiseReject);
  }

  this.Init = function () {
    var connection = mongoose.connect(mongoDBConnection, function (err) {
      if (err) {
        console.error(err);
      }

      for (var i = 0; i < DalModels.length; i++) {
        this.Models[DalModels[i].TableName] = DalModels[i].GetMongoModel();

      }
    }.bind(this));
  }.bind(this);

  this.GetBlankDataContext = function (nixObjectType) {
    return {
      "ID": uuid.v1(),
      "NixObjectType": nixObjectType,
      "NixDataObject": {
        "NixObjectType": nixObjectType,
      },
      "Filters": {},
      "success": true
    };
  };

  this.GetObjectModel = function (ObjectData) {
    if (ObjectData.NixObjectType == undefined) {
      return null;
    }

    return DalModels.getModel(ObjectData.NixObjectType);
  };

  this.GetModelList = function () {
    var modelList = [];
    for (var i = 0; i < DalModels.length; i++) {
      modelList.push({Name: DalModels[i].NixObjectType, Title: DalModels[i].TableName});
    }

    return modelList;
  };

  this.SetResponseError = function (requestObject, err, callBack) {
    requestObject.error = err;
    requestObject.false = true;
    callBack(err);
  };

  restifyHelper.AddPostService("/nixdata", function (req, res, next) {
    var dataRequest = req.body;

    if (Array.isArray(dataRequest)) {
      var loadCounter = dataRequest.length;
      for (var i = 0; i < loadCounter; i++) {
        this.GetAllObjects(dataRequest[i], function (results) {
          loadCounter--;
          if (loadCounter == 0) {
            restifyHelper.RespondSuccess(res, dataRequest);
            next();
          }
        }.bind(this));
      }
      ;
    }
    else {
      this.GetAllObjects(dataRequest, function (results) {
        restifyHelper.RespondSuccess(res, results);
        next();
      }.bind(this));
    }
  }.bind(this));

  restifyHelper.AddPostService("/nixdatasubmit", function (req, res, next) {
    this.SubmitNixObjects_Transaction(req.body, req.user.ID, function (results) {
      restifyHelper.RespondSuccess(res, results);
      next();
    }.bind(this));
  }.bind(this));

  /* Get objects */
  this.GetAllObjects = function (requestObject, callBack, disableListeners) {
    if (disableListeners == undefined) {
      disableListeners = false;
    }

    var model = this.GetObjectModel({NixObjectType: requestObject.NixObjectType});

    this.CallEvent(this.OnLoadBefore, requestObject, disableListeners, function (err) {
      if (err) {
        this.SetResponseError(requestObject, context.error, callBack);
      }


      var mongoModel = model.GetMongoModel();

      var mongoResult = function (err, results) {
        if (err) {
          this.SetResponseError(requestObject, err, callBack);
          return;
        }

        for (var i = 0; i < results.length; i++) {
          results[i] = results[i]._doc;
        }

        requestObject.success = true;
        requestObject.data = results;

        if (requestObject.TotalItems) {
          requestObject.data.TotalItems = requestObject.TotalItems;
        }

        this.CallEvent(this.OnLoadAfter, results, disableListeners, function (err) {
          if (err) {
            this.SetResponseError(requestObject, err, callBack);
            return;
          }

          callBack(results);
        }.bind(this));
      }.bind(this);

      var sort = {};
      if (requestObject.OrderBy) {
        var tmpSort = {};
        tmpSort[requestObject.OrderBy.field] = 1;

        if (requestObject.OrderBy.direction == "desc") {
          tmpSort[requestObject.OrderBy.field] = -1;
        }
        sort = tmpSort;
        // tmpcursor.sort(tmpSort);
      }

      if (requestObject.Paging) {
        var pageSkip = requestObject.Paging.page * requestObject.Paging.size;

        var tmpCount = mongoModel.count(model.getWhereStatement(requestObject.Filters), function (err, totalitems) {
          requestObject.TotalItems = totalitems;
          mongoModel.find(model.getWhereStatement(requestObject.Filters), mongoResult).skip(pageSkip).limit(requestObject.Paging.size).sort(sort);
          // mongoModel.find(requestObject.Filters,mongoResult).skip(pageSkip).limit(requestObject.Paging.size);
        });
      }
      else {
        mongoModel.find(model.getWhereStatement(requestObject.Filters), mongoResult).sort(sort);
      }


    }.bind(this));
  };

  this.GetAllDataContextes = function (dataContextes, callBack) {
    var loadCounter = dataContextes.length;

    for (var i = 0; i < loadCounter; i++) {
      this.GetAllObjects(dataContextes[i], function (res) {
        loadCounter--;
        if (loadCounter == 0)
          callBack(dataContextes);
      });
    }
  }.bind(this);


  this.submitNixObject = function (nixObject, callBack) {
    var NixObjectType = nixObject.NixObjectType;
    var model = this.GetObjectModel({NixObjectType: NixObjectType});
    var mongoModel = model.GetMongoModel();

    switch (nixObject.NixObjectState) {
      case Nix.Enum.NixDataObjectStates.New:
        var inserObj = model.CreateInsertStatement(nixObject);
        var newModel = new mongoModel(inserObj.Data);
        newModel.save(function (err, data) {
          data.insertedID = data.ID;
          callBack(err, [data]);
        });
        break;
      case Nix.Enum.NixDataObjectStates.Updated:
        var updateObj = model.CreateUpdateStatement(nixObject);

        mongoModel.findOne(updateObj.Filter, function (err, data) {
          if (err) {
            callBack(err, data);
            return;
          }
          var fields = Object.keys(updateObj.Data);
          for (var i = 0; i < fields.length; i++) {
            var tmpField = fields[i];
            data[tmpField] = updateObj.Data[tmpField];
          }

          data.save(function (err) {
            callBack(err);
          });
        });

        break;
      case Nix.Enum.NixDataObjectStates.Deleted:
        try {
          var delObj = model.CreateDeleteStatement(nixObject);

          mongoModel.remove(delObj.Filter, function (err, data) {
            if (err) {
              callBack(err, data);
              return;
            }

            callBack(err);
          });
        }
        catch (ex) {
          console.error(ex);
        }
        break;
      default:
        //log error
        break;
    }
  }


  /* Submit objects */
  this.submitNextNixObject_Transaction = function (nixObjects, userID, objectIndex, nixResults, callBack) {
    var ObjectData = nixObjects[objectIndex];
    var NixObjectType = ObjectData.NixObjectType;
    var model = this.GetObjectModel({NixObjectType: NixObjectType});

    this.submitNixObject(ObjectData, function (err, recordsets) {
      objectIndex++;

      var res = {
        NixObjectType: ObjectData.NixObjectType,
        NixObjectID: ObjectData.NixObjectID,
        Success: false,
        NixObjectState: ObjectData.NixObjectState,
        NixObjectExpired: false,
        mergeObject: {}
      };

      if (err) {
        res.Success = false;
        res.Error = err;
        res.NixObject = ObjectData;
      }
      else {
        res.Success = true;

        var objectId = model.GetIdentityValue(ObjectData);
        var objectData = model.GetAllObjectValues(ObjectData);

        if (ObjectData.NixObjectState == Nix.Enum.NixDataObjectStates.New) {
          res.mergeObject.ID = recordsets[0].insertedID;
          objectId = res.mergeObject.ID;
        }
        else if (ObjectData.NixObjectState == Nix.Enum.NixDataObjectStates.Deleted) {
          res.mergeObject.NixObjectExpired = true;
        }

        AppRegister.LogObjectSubmit(ObjectData.NixObjectType, ObjectData.NixObjectState, objectId, objectData, userID);
      }

      nixResults.push(res);
      if (objectIndex < nixObjects.length && res.Success) {
        this.submitNextNixObject_Transaction(nixObjects, userID, objectIndex, nixResults, callBack);
      }
      else {
        if (err) {
          if (!rolledBack) {
            transaction.rollback(function (err) {
              if (err) {
                console.error("Error on RollBack:" + JSON.stringify(err));
              }

              callBack(nixResults);
            });
          }
        } else {
          callBack(nixResults);
        }
      }

    }.bind(this));
  };

  this.SubmitNixObjects_Transaction = function (nixObjects, _userID, _callBack, disableListeners) {

    if (disableListeners == undefined) {
      disableListeners = false;
    }

    var callBack = _callBack;
    var userID = _userID;

    if (_callBack == undefined && (typeof _userID) == "function") {
      userID = null;
      callBack = _userID;
    }

    FireOnSubmitBeforeEvent(disableListeners, nixObjects, function () {
      this.submitNextNixObject_Transaction(nixObjects, userID, 0, [], function (results) {
        FireOnSubmitAfterEvent(disableListeners, nixObjects, function () {
          callBack(results);
        }.bind(this), callBack)
      }.bind(this));

    }.bind(this), callBack);
  };
}



