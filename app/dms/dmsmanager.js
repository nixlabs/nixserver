"use strict";
var mime = require('mime');
//This is a template module which is ignored on modules load. Use this file as a reference of Module Structure

module.exports = function(){
    this.ModuleName = "DMS Manager";
    this.ModulePriority = -70;

    var restifyHelper = global.RestifyHelper;
    var appRegister = global.Nix.AppRegister;


    var dms_localModules = require('./dms_local.js');

    var dms_local = new dms_localModules();
    var dms_provider = null;
    if(Nix.Config.dmsConfig.type=="local"){
        dms_provider = dms_local;
    }
    else if(Nix.Config.dmsConfig.type=="gdrive") {
        var dms_gdriveModule = require('./dms_gdrive.js');
        dms_provider = new dms_gdriveModule();
    }

    this.Init = function(){
        var promise = new Promise(this.CheckDB);
        return promise;
    };

    this.CheckDB = function (promiseResolve, promiseReject) {
        appRegister.AddModel('NixFile', {
            Filename: String,
            RefName: String,
            ObjectType: String,
            ObjectID: Number,
            PhysicalPath:String,
            Size:String,
            Extension: String,
            DateUploaded: Date
        }, true);

        appRegister.AddModel('NixFolder', {
            Path: String,
            ObjectType: String,
            ObjectID: Number,
            DateCreated: Date
        }, true);

        this.InitProviders(promiseResolve, promiseReject);
    }.bind(this);

    this.InitProviders = function(promiseResolve, promiseReject){
        var localPromise = dms_local.Init();
        var providerPromise = null;
        if(dms_local != dms_provider){
            providerPromise = dms_provider.Init();
        }

        localPromise.then(function(){
            localPromise.then(promiseResolve, promiseReject);
        },promiseReject);
    };



    restifyHelper.AddPostService("/dms/upload", function (req, res, next) {
        var dataObj = JSON.parse(req.body.data);
        var NixObjectType=  dataObj.NixObjectType;
        var ObjectID = dataObj.ObjectID;
        var file = req.files.file;

        dms_provider.UploadFile(file, NixObjectType, ObjectID, function(err, fileInfo){
            if(err){
                restifyHelper.RespondError(res, 404, "Error on uploading files");
            }
            else{
                restifyHelper.RespondSuccess(res, fileInfo );
            }
        });

        next();
    });



    restifyHelper.AddPostService("/dms/delete/:FileID", function (req, res, next) {
        var FileID = req.params.FileID;

        dms_provider.DeleteFile(FileID, function(err){
            if(err){
                restifyHelper.RespondError(res, 404, "Error on deleting File");
            }
            else{
                restifyHelper.RespondSuccess(res, true );
            }
        });

        next();
    });

    restifyHelper.AddGetService("/dms/fileinfo/:FileID", function (req, res, next) {
        var FileID = req.params.FileID;

        dms_provider.GetFileInfo(FileID, function(err, fileInfo){
            if(err){
                restifyHelper.RespondError(res, 404, "Error on getting File info");
            }
            else{
                restifyHelper.RespondSuccess(res, fileInfo);
            }
        });

        next();
    });

    restifyHelper.AddGetService("/dms/objectfiles/:NixObjectType/:ObjectID", function (req, res, next) {
        var NixObjectType=  req.params.NixObjectType;
        var ObjectID = req.params.ObjectID;

        dms_provider.GetAllFiles(NixObjectType, ObjectID, function(err, fileInfo){
            if(err){
                restifyHelper.RespondError(res, 404, "Error on getting Files info");
            }
            else{
                restifyHelper.RespondSuccess(res, fileInfo);
            }
        });

        next();
    });


    restifyHelper.AddGetService("/dms/download/:FileID", function (req, res, next) {
        dms_provider.DownloadFile(req.params.FileID, function(err, data){
            if(err){
                restifyHelper.RespondError(res, 404, "Error on getting Files info");
                return;
            }
            else if(data == null){
                restifyHelper.RespondError(res, 404, "Invalid file.");
                return;
            }

            res.setHeader('Content-Type', mime.lookup(data.Filename));
            res.setHeader('Content-Disposition', 'attachment; filename=' + data.Filename);
            res.writeHead(200);
            res.end(data.dataStream);

            return next();

            //
            // res.setHeader('Content-Type', 'application/octet-stream')
            // res.setHeader('Content-Length', Buffer.byteLength(data.dataStream));
            // res.setHeader('Content-Disposition', 'attachment; filename=' + data.Filename);
            //
            // res.writeHead(200);
            // res.end(data.dataStream);
            // next();
        });

    },false);
    
    this.UploadFile = function (file, NixObjectType, ObjectID, callBack) {
        dms_provider.UploadFile(file, NixObjectType, ObjectID, callBack);
    }



}