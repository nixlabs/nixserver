"use strict";
//This is a template module which is ignored on modules load. Use this file as a reference of Module Structure

var fs = require('fs');
var uuid = require('node-uuid');

module.exports = function () {
    this.ModuleName = "DMS Local FS";

    var restifyHelper = global.RestifyHelper;
    var appRegister = global.Nix.AppRegister;
    var dalManager = global.Nix.DalManager;

    var folderBased = {};

    this.Init = function () {
        var promise = new Promise(this.CheckFolders);
        return promise;
    }

    this.CheckFolders = function (promiseResolve, promiseReject) {
        var promiseFolderBased;
        var promiseUploadsFolder = new Promise(this.CheckUploadsFolder)

        if (Nix.Config.dmsConfig.type == "local") {
            var promiseFolderBased = new Promise(this.CheckFolderBasedObjects);
        }

        promiseUploadsFolder.then(function () {
            if (promiseFolderBased) {
                promiseFolderBased.then(promiseResolve, promiseReject);
            }
            else {
                promiseResolve();
            }
        }, promiseReject)
    }.bind(this);

    this.CheckUploadsFolder = function (promiseResolve, promiseReject) {
        if (!fs.existsSync(Nix.Config.dmsConfig.folderpath)) {
            var err = fs.mkdirSync(Nix.Config.dmsConfig.folderpath);

            if (err) {
                promiseReject("Upload dir cannot be created, error: " + err);
            }
            else {
                promiseResolve();
            }
        }
        else {
            promiseResolve();
        }
    };

    this.CheckFolderBasedObjects = function (promiseResolve, promiseReject) {
        if (Nix.Config.dmsConfig.folderbased) {
            for (var i = 0; i < Nix.Config.dmsConfig.folderbased.length; i++) {
                var tmpFB = Nix.Config.dmsConfig.folderbased[i];
                folderBased[tmpFB.objecttype] = tmpFB;

                var path = Nix.Config.dmsConfig.folderpath + '/' + tmpFB.rootpath;

                if (fs.existsSync(path) == false) {
                    var err = fs.mkdirSync(path);
                    if (err) {
                        promiseReject("Object folder could not be created, error: " + err);
                        return;
                    }
                }
            }
        }

        promiseResolve();
    };


    this.GetObjectPath = function (folderBasedInfo, dataObj) {
        var folderPath = Nix.Config.dmsConfig.folderpath + "/" + folderBasedInfo.rootpath + "/";

        var FolderNaming = "";
        var FolderNamingArgs = folderBasedInfo.foldernaming.split(/[\s{}]+/);

        for (var i = 0; i < FolderNamingArgs.length; i++) {
            var Arg = FolderNamingArgs[i];
            var tmpArg = Arg.split(".");
            if (tmpArg.length > 1) {
                if (dataObj[tmpArg[0]] != undefined) {
                    if (tmpArg[1].indexOf("()") != -1) {
                        var functionArg = tmpArg[1].substring(0, tmpArg[1].indexOf("()"));
                        FolderNaming += dataObj[tmpArg[0]][functionArg]();
                    }
                    else {
                        FolderNaming += dataObj[tmpArg[0]][tmpArg[1]];
                    }
                }
            }
            else {
                if (dataObj[Arg] != undefined) {
                    FolderNaming += dataObj[Arg];
                }
                else if (Arg != "/") {
                    FolderNaming += Arg;
                }
            }
            if (Arg == "/") {
                if (!fs.existsSync(folderPath + FolderNaming)) {
                    var err = fs.mkdirSync(folderPath + FolderNaming);

                    if (err) {
                        console.log(FolderNaming + " dirs cannot be created, error: " + err);
                    }
                    else {
                        console.log(FolderNaming + " created ");
                    }
                }
                FolderNaming += "/";
            }

            //Algoritham for creating folders
        }

        return folderPath + FolderNaming;

        // folderPath += dataObj.ID + "-" + dataObj.Name;


    }


    this.UploadFile = function (File, NixObjectType, ObjectID, callBack) {


        var DataContext = dalManager.GetBlankDataContext(NixObjectType);
        DataContext.Filters.ID = ObjectID;

        dalManager.GetAllDataContextes([DataContext], function (ress) {
            var dataObj = DataContext.data[0];


            var folderPath = Nix.Config.dmsConfig.folderpath;
            var fileNaming = Nix.Enum.FileNamingTypes.Random;

            var fileExtension = "";

            var tmpSeg = File.name.split(".");
            if (tmpSeg.length > 1) {
                fileExtension = tmpSeg[tmpSeg.length - 1];
            }

            var fileName = File.name.substring(0, (File.name.length - fileExtension.length));

            if (fileName.endsWith('.')) {
                fileName = fileName.substring(0, fileName.length - 1);
            }

            if (folderBased[NixObjectType] != undefined) {
                folderPath = this.GetObjectPath(folderBased[NixObjectType], dataObj);
                fileNaming = folderBased[NixObjectType].namingmode;
            }

            if (fileNaming == Nix.Enum.FileNamingTypes.Random) {
                fileName = uuid.v1();
                fileName = fileName.replace(new RegExp('-', 'g'), '');
            }

            var created = false;
            var fileIndex = 0;
            while (created == false) {
                var tmpFileName = fileName;

                if (fileIndex > 0) {
                    tmpFileName = fileName + "(" + fileIndex + ")";
                }

                if (fileExtension.length > 0) {
                    tmpFileName += "." + fileExtension;
                }

                if (fs.existsSync(folderPath)) {

                    if (fs.existsSync(folderPath + '/' + tmpFileName) == false) {

                        fs.readFile(File.path, function (err, data) {
                            if (err) {
                                callBack(err);
                                return;
                            }

                            fs.writeFile(folderPath + '/' + tmpFileName, data, function (err) {
                                if (err) {
                                    callBack(err);
                                    return;
                                }
                                var newFile = new appRegister.Models.NixFile({
                                    Filename: File.name,
                                    RefName: tmpFileName,
                                    ObjectType: NixObjectType,
                                    ObjectID: ObjectID,
                                    PhysicalPath: folderPath,
                                    Extension: fileExtension,
                                    DateUploaded: new Date()
                                });
                                newFile.save(function (err, data) {
                                    callBack(null, data._doc);
                                });
                                //save file in mongo and return fileinfo
                            });
                        });

                        created = true;
                    }

                    fileIndex++;
                }
            }

        }.bind(this));


    }

    this.DeleteFile = function (FileID, callBack) {
        appRegister.Models.NixFile.find({ID: FileID}, function (err, fileInfo) {
            if (fileInfo[0]._doc) {
                fileInfo = fileInfo[0]._doc;
                fs.unlinkSync(fileInfo.PhysicalPath+ "/" + fileInfo.RefName);
                appRegister.Models.NixFile.remove({ID:fileInfo.ID},callBack)
            }
        });
    };

    this.DownloadFile = function (FileID, callBack) {
        appRegister.Models.NixFile.findOne({ID: FileID}, function(err, file){
            if(err){
                callBack(err);
                return;
            }

            if(file != null){
                fs.readFile(file.PhysicalPath + "/" + file.RefName, function(err, data){
                    file.dataStream = data;
                    callBack(null, file);
                });
            }
            else{
                callBack(null,null);
            }

        });
    };

    this.GetFileInfo = function (FileID, callBack) {
        appRegister.Models.NixFile.findOne({ID: FileID}, callBack);
    };

    this.GetAllFiles = function (NixObjectType, ObjectID, callBack) {
        appRegister.Models.NixFile.find({ObjectType: NixObjectType, ObjectID: ObjectID}, callBack);
    }

}
