﻿module.exports = function () {
    /*
     * Required modules
     */
    var dalManager = global.DalManager;
    
    var path = require('path');
    var request = require('request');
    var fs = require('fs');
    var readline = require('readline');
    var google = require('googleapis');
    var googleAuth = require('google-auth-library');
    
    var config = require('config');
    var googleDriveConfig = config.get('googleDrive');
    
    var rootPath = path.resolve(__dirname);
    

    var SCOPES = ['https://www.googleapis.com/auth/drive'];
    var TOKEN_DIR = rootPath + '/../' + googleDriveConfig.token_dir;
    
    var TOKEN_PATH = TOKEN_DIR + googleDriveConfig.drive_json_filename;
    
    var fullUrl = "https://www.googleapis.com/drive/v2/files";

    /*
     * Google Drive Authentication
     */
    function authorize(credentials, callback) {
        var clientSecret = credentials.installed.client_secret;
        var clientId = credentials.installed.client_id;
        var redirectUrl = credentials.installed.redirect_uris[0];
        var auth = new googleAuth();
        var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);
        
        // Check if we have previously stored a token.
        fs.readFile(TOKEN_PATH, function (err, token) {
            if (err) {
                getNewToken(oauth2Client, callback);
            } else {
                oauth2Client.credentials = JSON.parse(token);
                callback(oauth2Client);
            }
        });
    }
    function getNewToken(oauth2Client, callback) {
        var authUrl = oauth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: SCOPES
        });
        console.log('Authorize this app by visiting this url: ', authUrl);
        var rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        rl.question('Enter the code from that page here: ', function (code) {
            rl.close();
            oauth2Client.getToken(code, function (err, token) {
                if (err) {
                    console.log('Error while trying to retrieve access token', err);
                    return;
                }
                oauth2Client.credentials = token;
                storeToken(token);
                callback(oauth2Client);
            });
        });
    }
    function storeToken(token) {
        try {
            fs.mkdirSync(TOKEN_DIR);
        } catch (err) {
            if (err.code != 'EEXIST') {
                throw err;
            }
        }
        fs.writeFile(TOKEN_PATH, JSON.stringify(token));
        console.log('Token stored to ' + TOKEN_PATH);
    }
    
    
    /*
     * Google Drive Services
     */
    var service = google.drive('v2');

    this.googleAuth = null;
    this.templateFolderObj = null;
    this.contractFolderObj = null;
    this.IsInit = false;
    
    this.NixObjectTypes = {
        Template: 15,
        ContractDocument : 17
    };

    this.Init = function (auth) {
        this.googleAuth = auth;
        //dalManager.OnSubmitBefore = this.OnSubmitBefore;
        //dalManager.OnSubmitAfter = this.OnSubmitAfter;
        dalManager.OnLoadBefore = this.OnLoadBefore;
        dalManager.OnLoadAfter = this.OnLoadAfter;
    }.bind(this);
    
    this.GetInitData = function (callback) {
        service.files.list({
            auth: this.googleAuth,
            maxResults: 1000,
            q: "trashed=false and mimeType='application/vnd.google-apps.folder'"
        }, function (err, response) {
            var resp = new ResultObject();
            if (err) {
                console.log('The API returned an error: ' + err);
                resp.error = err;
            } else {
                var items = response.items;
                var count = 0;
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    if (item.title === 'template') {
                        this.templateFolderObj = item;
                        console.log("Template Folder found!");
                        count++;
                    }
                    if (item.title === 'contract') {
                        this.contractFolderObj = item;
                        console.log("Contract Folder found!");
                        count++;
                    }
                    if (count == 2)
                        break;
                }
                if (count == 2) {
                    resp.success = true;
                    this.IsInit = true;
                } else {
                    console.log("Folder not found!");
                    resp.error = "Folder not found";
                }
            }
            callback(resp);
        }.bind(this));
    }.bind(this);

    /*
     * Get All Template
     */
    this.GetAllTemplatesFromFolder = function (callback) {
        var TemplateID = this.templateFolderObj.id;
        service.files.list({
            auth: this.googleAuth,
            maxResults: 1000,
            q: "trashed=false and mimeType!='application/vnd.google-apps.folder' and '" + TemplateID + "' in parents"
        }, function (err, response) {
            var resp = new ResultObject();
            if (err) {
                console.log('The API returned an error: ' + err);
                resp.error = err;
            } else {
                var items = response.items;
                resp.success = items;
            }
            callback(resp);
        }.bind(this));
    }.bind(this);
    
    this.CreateContractFolder = function (contractID, callback) { 
        
        var headers = { 'Authorization': this.googleAuth.credentials.token_type + ' ' + this.googleAuth.credentials.access_token };
        
        var filename = "Contract" + contractID;
        
        var parentID = this.contractFolderObj.id;

        var bodyData = {
            "title": filename,
            "parents": [{ "id": parentID }],
            "mimeType": "application/vnd.google-apps.folder"
        };
        
        var post_options = {
            method: 'POST',
            url: fullUrl,
            json: bodyData,
            headers: headers
        };
        
        request(post_options, function (error, response, body) {
            var resp = new ResultObject();
            if (error != null) {
                resp.error = error;
            } else {
                resp.success = body;
            }
            callback(resp);
        });
    }.bind(this);

    this.CopyTemplateToContractFolder = function (templateID, contractID, filename, callback) {
        this.GetFileById(templateID, (function (response) {
            if (response.error != null) {
                callback(response);
            } else {
                var newFile = response.success;
                newFile.parents = [{ id: contractID }];
                newFile.title = filename;
                service.files.copy({
                    auth: this.googleAuth,
                    fileId: templateID,
                    resource: newFile
                }, function (err, response) {
                    var resp = new ResultObject();
                    if (err) {
                        console.log('The API returned an error: ' + err);
                        resp.error = err;
                    } else {
                        console.log("File copied!");
                        resp.success = response;
                    }
                    callback(resp);
                });
            }
        }).bind(this), true);
    }.bind(this);
    
    this.GetFileById = function (fileId, callback) {
        service.files.get({
            auth: this.googleAuth,
            fileId: fileId
        }, function (err, response) {
            var resp = new ResultObject();
            if (err) {
                console.log('The API returned an error: ' + err);
                resp.error = err;
            } else {
                resp.success = response;
                console.log("File by id found!");
            }
            
            callback(resp);
        });
    }.bind(this);
    
    this.GetFileByName = function (filename, callback) {
        var query = "title='" + filename + "'";
        service.files.list({
            auth: this.googleAuth,
            q: query
        }, function (err, response) {
            var resp = new ResultObject();
            if (err) {
                console.log('The API returned an error: ' + err);
                resp.error = err;
            } else {
                var items = response.items;
                console.log(items.length + ' files found.');
                if (items.length == 0)
                    resp.success = null;
                else
                    resp.success = items[0];
            }
            callback(resp);
        });
    }.bind(this);

    /*
     * Database Methods
     */
    
    
    /*
     * Add New Contract Document
     */
    this.AddNewContractDocument = function (nixObjects, callBack) {
        if (nixObjects.length > 0) {
            var TemplateGoogleID = nixObjects[0].TemplateGoogleID;
            var Filename = nixObjects[0].FileName;
            var ContractID = nixObjects[0].ContractID;
            var ContractFolder = null;
            
            if (this.IsInit) {
                this.AddNewContractDocumentFunc(ContractID, TemplateGoogleID, ContractFolder, Filename, function (newContractDoc) { 
                    callBack(newContractDoc);
                });
            } else { 
                this.GetInitData((function (initRes) {
                    if (initRes.error != null) { 
                
                    } else {
                        this.AddNewContractDocumentFunc(ContractID, TemplateGoogleID, Filename, function (newContractDoc) {
                            callBack(newContractDoc);
                        });
                    }
                }).bind(this), true);
            
            }
        } else {
            //error
            callBack();
        }
    }.bind(this);
    
    this.AddNewContractDocumentFunc = function (ContractID, TemplateGoogleID, Filename, callBack) { 
        var ContractFolder = null;
        this.GetFileByName("Contract" + ContractID, (function (contract) {
            if (contract.error != null) { 
                    //error
            } else {
                var newObj = {
                    ContractID: 0,
                    FileName: "",
                    URL: "",
                    GoogleID: "",
                    Type: "",
                    TemplateGoogleID: "",
                    NixObjectState: "new",
                    Enabled: false,
                    NixObjectType: this.NixObjectTypes.ContractDocument
                };

                if (contract.success != null) {
                    ContractFolder = contract.success;
                    
                    this.CopyTemplateToContractFolder(TemplateGoogleID, ContractFolder.id, Filename, (function (result) {
                        if (result.error != null) { 
                        
                        } else { 
                            newObj.ContractID = ContractID;
                            newObj.FileName = result.success.title;
                            newObj.URL = result.success.alternateLink;
                            newObj.Type = "";
                            newObj.GoogleID = result.success.id;
                            newObj.TemplateGoogleID = TemplateGoogleID;
                            var objList = [];
                            objList.push(newObj);
                            //write to db
                            dalManager.SubmitNixObjects_Transaction(objList, (function (res) {
                                callBack(res);
                            }).bind(this), true);
                        }
                    }).bind(this), true);
                } else {
                    //create contract folder
                    this.CreateContractFolder(ContractID, (function (newContract) {
                        if (newContract.error != null) { 
                            
                        } else {
                            ContractFolder = newContract.success;
                            this.CopyTemplateToContractFolder(TemplateGoogleID, ContractFolder.id, Filename, (function (result) {
                                if (result.error != null) { 
                        
                                } else {
                                    newObj.ContractID = ContractID;
                                    newObj.FileName = result.success.title;
                                    newObj.URL = result.success.alternateLink;
                                    newObj.Type = "";
                                    newObj.GoogleID = result.success.id;
                                    newObj.TemplateGoogleID = TemplateGoogleID;
                                    var objList = [];
                                    objList.push(newObj);
                                    //write to db
                                    dalManager.SubmitNixObjects_Transaction(objList, (function (res) {
                                        callBack(res);
                                    }).bind(this), true);
                                }
                            }).bind(this), true);
                        }
                    }).bind(this), true);
                }
            }
        }).bind(this), true);
    }.bind(this);

    this.GetObjectByID = function (requestObject, callBack) {
        dalManager.GetObjectByID(requestObject, function (res) {
            callback(res);
        });
    }.bind(this);

    this.RefreshTemplates = function (callBack) { 
        this.SyncTemplates(function (res) {
            callBack(res);
        });
    }.bind(this);
    
    this.GetSyncedTemplates = function (callBack) { 
        this.SyncTemplates(function (res) {
            callBack();
        });
    }.bind(this);
    
    this.SyncTemplatesFunc = function (requestObject, callBack) { 
        
        this.GetAllTemplatesFromFolder(function (result) {
            if (result.error != null) {
                callBack(result.error);
                return;
            }
            
            var items = result.success;
            
            dalManager.GetAllObjects(requestObject, function (res) {
                
                var newObjList = [];
                var upObjList = [];
                var delObjList = [];
                var objList = [];
                
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    item.new = true;
                    item.changed = false;
                    var counter = 0;
                    for (var j = 0; j < res.length; j++) {
                        counter++;
                        var dbItem = res[j];
                        if (dbItem.GoogleID == item.id) {
                            // add updated item
                            var upObj = {
                                ID: dbItem.ID,
                                Name: item.title,
                                URL: item.alternateLink,
                                GoogleID: item.id,
                                NixObjectState: "updated",
                                NixObjectType: requestObject.NixObjectType
                            };
                            item.new = false;
                            item.changed = dbItem.URL != item.alternateLink;
                            if (item.changed) {
                                objList.push(upObj);
                                continue;
                            }
                            item.changed = dbItem.Name != item.title;
                            if (item.changed) {
                                objList.push(upObj);
                                continue;
                            }
                        }
                        if (counter == res.length) {
                            //add new item
                            if (item.new) {
                                var newObj = {
                                    Name: item.title,
                                    URL: item.alternateLink,
                                    GoogleID: item.id,
                                    NixObjectState: "new",
                                    Enabled: false,
                                    NixObjectType: requestObject.NixObjectType
                                };
                                objList.push(newObj);
                            }
                        }
                    }
                }
                
                for (var i = 0; i < res.length; i++) {
                    var dbItem = res[i];
                    var counter = 0;
                    for (var j = 0; j < items.length; j++) {
                        counter++;
                        var item = items[j];
                        if (item.id == dbItem.GoogleID)
                            break;
                        
                        if (counter == items.length) {
                            var delObj = {
                                ID: dbItem.ID,
                                NixObjectState: "deleted",
                                NixObjectType: requestObject.NixObjectType
                            };
                            objList.push(delObj);
                        }
                    }
                }
                
                if (objList.length > 0) {
                    dalManager.SubmitNixObjects_Transaction(objList, function (res) {
                        dalManager.GetAllObjects(requestObject, function (res) {
                            callBack(res)
                        })
                    }.bind(this), true);
                }
                else {
                    callBack(res);
                }

            }.bind(this), true);


        }.bind(this));
    }.bind(this);
    
    this.SyncTemplates = function (callBack) {
        
        var requestObject = this.CreateRequestObjectByType(this.NixObjectTypes.Template);
        
        if (this.IsInit) {
            this.SyncTemplatesFunc(requestObject, function (results) { 
                callBack(results);
            });
        } else {
            this.GetInitData(function (initRes) {
                if (initRes.error != null) { 
                
                } else {
                    this.SyncTemplatesFunc(requestObject, function (results) {
                        callBack(results);
                    });
                }
            }.bind(this));
        }


    }.bind(this);
    
    this.CreateRequestObjectByType = function (type) { 
        return { "ID":"4dd685e9-818c-289b-1507-1e2d9ff1e244","NixObjectType": type,"Filters": { },"Paging": { },"DataLoadOptions":[],"Data":[]};
    };
    
    /*
     * Service Events
     */
    this.OnSubmitBefore = function () {
    }.bind(this);
    
    this.OnSubmitAfter = function () {

    }.bind(this);
    
    this.OnLoadBefore = function (requestObject, callBack) {
        if (requestObject.NixObjectType == this.NixObjectTypes.Template && Object.keys(requestObject.Filters).length == 0) {
            
            this.GetSyncedTemplates(function (res) {
                callBack();
            });

        }
        else if (requestObject.NixObjectType == this.NixObjectTypes.ContractDocument && Object.keys(requestObject.Filters).length == 0) {
            callBack();
        } else {
            callBack();
        }

        //on error call callBack(error);
        //on success call callBack();
    }.bind(this);
    
    this.OnLoadAfter = function (result, callBack) {
        callBack();
        //on error call callBack(error);
        //on success call callBack();
    }.bind(this);
    
    /*
     * Result Object Constructor
     */
    function ResultObject() {
        this.success = null;
        this.error = null;
    };





    this.UploadFile = function(File, NixObjectType, ObjectID, Options){

    }

    this.DeleteFile = function(FileID){

    }

    this.DownloadFile = function(FileID){

    }

    this.GetFileInfo = function(FileID){

    }

    this.GetAllFiles = function(NixObjectType, ObjectID){

    }

    /*
     * Initialization
     */
    authorize(googleDriveConfig.secret, this.Init);
};