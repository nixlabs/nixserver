"use strict";

var fs = require('fs');
var jsonfile = require('jsonfile');
var decomment = require('decomment');

var path = require('path');

module.exports = function () {
    this.ModuleName = "NixConfigHelper";
    var configPath = BasePath + path.sep + "config" + path.sep + "default.json";
    var samplePath = BasePath + path.sep + "config" + path.sep + "default.sample.json";
    var addedFields = [];
    
    if (fs.existsSync(configPath) && fs.existsSync(samplePath)) {
        try {
            var config = fs.readFileSync(configPath, "utf-8");
            var sample = fs.readFileSync(samplePath, "utf-8");
            config = decomment(config);
            sample = decomment(sample);
            config = JSON.parse(config);
            sample = JSON.parse(sample);
            config = syncValues(sample, config, "");

            if (addedFields.length > 0) {
                try {
                    jsonfile.writeFileSync(configPath, config);
                    console.error("Config updated. Added Fields: " + addedFields.toString());
                }
                catch (ex) {
                    console.error("Failed to update config. Reason: " + ex);
                    process.exit();
                }
            }
            else
                console.log("Config update not needed.");
        }
        catch (ex) {
            console.error("Couldn't read config. Reason: " + ex);
            process.exit();
        }
    }
    else {
        if (fs.existsSync(samplePath)) {
            console.error("Config not found. Creating new one.");
            try {
                var sample = JSON.parse(fs.readFileSync(samplePath, "utf-8"));
                try {
                    jsonfile.writeFileSync(configPath, sample);
                    console.error("Config created from Sample.");
                }
                catch (ex) {
                    console.error("Couldn't create config from Sample. Reason: " + ex);
                    process.exit();
                }
            }
            catch (ex) {
                console.error("Couldn't read config sample. Reason: " + ex);
                process.exit();
            }
        }
        else {
            console.error("Sample Config not found please create one.");
            process.exit();
        }
    }

    function syncValues (sample, config, root) {
        var configKeys = Object.keys(config);
        for (var field in sample) {
            if (configKeys.indexOf(field) == -1) {
                config[field] = sample[field];
                addedFields.push(root + "[" + field + "]");
            }
            else {
                if (Array.isArray(sample[field])) {
                    for (var i = 0; i < sample[field].length; i++) {
                        var obj = sample[field][i];
                        if (typeof obj === 'object' && obj !== null)
                            config[field][i] = syncValues(sample[field][i], config[field][i], root + "["+ i + "][" + field + "]");
                    }
                }
                if (typeof sample[field] === 'object' && sample[field] !== null) {
                    config[field] = syncValues(sample[field], config[field], root + "[" + field + "]");
                }
            }
        }
        return config;
    }
};